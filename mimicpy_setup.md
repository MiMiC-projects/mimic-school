MiMiCPy is the companion Python library to MiMiC developed by Bharath Raghavan [![Orcid](https://info.orcid.org/wp-content/uploads/2019/11/orcid_16x16.png)](https://orcid.org/0000-0003-0186-2625) and Florian Schackert [![Orcid](https://info.orcid.org/wp-content/uploads/2019/11/orcid_16x16.png)](https://orcid.org/0000-0001-6028-3717).

For more information, refer to the information in the [MiMiCPy GitLab repository](https://gitlab.com/MiMiC-projects/mimicpy "MiMiCPy").

# Installation
We provide here two analogous ways two install MimICPy, one based on Python virtual environment, and the second one on conda environments.
## Install using a Python virtual environment
Python has builtin support for virtual environments that can be created and activated as follows
```
module load python
python -m venv mimicpy_venv
. mimicpy_venv/bin/activate
```
This will create and activate a virtual environment that will be located in the directory `mimicpy_venv` which is created in the directory from where you run the commands. You can, in principle, choose any name for the environment but avoid using names that clash with Python package names. The virtual environment can be deactivated by running the command
```
deactivate
```

To install MiMiCPy, use `pip` from within the virtual environment (i.e., after activating it)
```
python -m pip install git+https://gitlab.com/MiMiC-projects/mimicpy
```
To install with PyMOL and/or VMD support
```
PYMOLDIR="$HOME" VMDDIR="$HOME" python -m pip install git+https://gitlab.com/MiMiC-projects/mimicpy
```
This will write (or append to file, if it already exists) a `.pymolrc.py` and/or `.vmd.rc` (`vmd.rc` on Windows) file to the given directory (in this case `$HOME`). If these files are in the current directory, the user home directory, or the installation directory of the visualization package, they will be read and loaded on startup by PyMOL and VMD. This makes the `prepqm` command available to them.

:warning: Before running `mimicpy prepqm`, make sure that the GROMACS has been sourced, i.e. available on the command line. This is because MiMiCPy requires the `GMXDATA` environment variable to be set, so that it can access the force field folder in the GROMACS installation.
In particular, you can set the environment variable with the following command
```
export GMXLIB='/usr/share/gromacs/'
```
where the path is the one where the `top/` folder is located (where files are present).
In our case, this folder is `/g100_work/tra22_qmmm/Programs/GROMACS/share/gromacs`.
<!--- Specific path for CECAM school--->

## Install using a conda environment
Conda allows the user to create, export, list, remove, and update environments that have different versions of Python and/or packages installed in them. If you are not familiar with conda, check the [conda installation instructions](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html "installation") for details about the installation and the [conda environments documentation](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html "conda") for more information about conda environments in general.

Set up a conda environment for MiMiCPy and install it via `pip` 
```
module load conda 
conda create --name mimicpy
conda activate mimicpy
python -m pip install git+https://gitlab.com/MiMiC-projects/mimicpy
```

Alternatively, if this doesn't work, you can do:
```
conda create --name mimicpy
conda activate mimicpy
git clone https://gitlab.com/MiMiC-projects/mimicpy.git
cd mimicpy
python -m pip install .
```

To install with PyMOL and/or VMD support, pass the plugin path to PYMOLDIR or VMDDIR. This path is usually either the path to PyMOL/VMD installation folder, or the user home directory. For example,
```
PYMOLDIR="/home/user/" VMDDIR="/home/user/" python -m pip install git+https://gitlab.com/MiMiC-projects/mimicpy
```
This will write (or append to file, if it already exists) a `.pymolrc.py` and/or `.vmd.rc` (`vmd.rc` on Windows) file to the given directory. If these files are in the current directory, the user home directory, or the installation directory of the visualization package, they will be read and loaded on startup by PyMOL and VMD. This makes the `prepqm` command available to them.

Now that a `mimicpy` installation has been created, you can activate or deactivate the environment via 
```
conda activate mimicpy
conda deactivate
```
To install with PyMOL and/or VMD support
```
PYMOLDIR="$HOME" VMDDIR="$HOME" python -m pip install git+https://gitlab.com/MiMiC-projects/mimicpy
```
This will write (or append to file, if it already exists) a `.pymolrc.py` and/or `.vmd.rc` (`vmd.rc` on Windows) file to the given directory (in this case `$HOME`). If these files are in the current directory, the user home directory, or the installation directory of the visualization package, they will be read and loaded on startup by PyMOL and VMD. This makes the `prepqm` command available to them.

:warning: Before running `mimicpy prepqm`, make sure that the GROMACS has been sourced, i.e. available on the command line. This is because MiMiCPy requires the `GMXDATA` environment variable to be set, so that it can access the force field folder in the GROMACS installation.
In particular, you can set the environment variable with the following command
```
export GMXLIB='/usr/share/gromacs/'
```
where the path is the one where the `top/` folder is located (where files are present).
In our case, this folder is `/g100_work/tra22_qmmm/Programs/GROMACS/share/gromacs`.
<!--- Specific path for CECAM school--->

# Running MiMiCPy
To check that MiMiCPy has been correctly installed, after activating the Python virtual environment or conda environment you can access the `help` via
```
mimicpy --help
```
which should produce the following output:
```
 	                ***** MiMiCPy *****                  

 	 For more information type mimicpy [subcommand] --help 

usage: mimicpy [-h]  ...

optional arguments:
  -h, --help   show this help message and exit

valid subcommands:
  
    prepqm     create CPMD/MiMiC input and GROMACS index files
    prepmm     create/fix GROMACS MDP script for MiMiC run
    cpmd2coords
               convert CPMD/MiMiC input to coordinates
    fixtop     fix [ atomtypes ] section of GROMACS topology
    getmpt     create MiMiCPy topology from GROMACS topology
```
