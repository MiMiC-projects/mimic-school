# Title 
Main goal of exerice.

Subtasks for this exercise:
1. example: Energy minimization on the initial structure, needed to minimize the energy with the selected force field of the starting strucutre of the solvated system. This step will for example improve the orientation of hydrogen bonds. 
2. etc.
3. etc.
 
:warning: Important technical prerequisites to run the tutorial, ex. running version of MiMiC, CPMD and GROMACS (see acetone tutorial for an example).  

:warning: To run the tutorial clone the git repository in your local machine. Input files needed for this tutorial are located in the `mimic/examples/acetone/data/` folder.

:information_source: Tutorial based on:
* Important resources used for the preparation of the tutorual, if any
* etc.

:information_source: Main contributors to the updated version of this tutorial:  
* Name Surname [![Orcid](./data/fig/orcid.png)](https://orcid.org/XXXXXX)
* etc.
