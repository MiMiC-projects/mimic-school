# Title
short description of the tutorialgoals
 
## Subtitle for first subtask
Instructions to follow, such as making a directory or running a script. 
Show code below:

```
code that can be copy/pasted in command line 
multiline accepted
```

If we refer to specific file we like to highlight the file name like:  `file.mdp` file. Same highlight for input commands or keywords.

:warning: Important steps can be highlighted with a warning  

Below is an example of when you could use a drop down function to add more detailed information that may be useful to new users but could be skipped by more advanced users (for example we did that for the details of gromacs/cpmd input files, which can be skipped by users already familiar with them). 

<details>
<summary>:information_source: topic name</summary>
<hr>   
Write more info here as you like!
 
```
even code supported
```
<hr>   
</details>

<!--
if you need to write unseen comments to yourself/others in the md code use this structure ;)
-->

## Subtitle for next subtask 
Instructions.

### Subsubtitle 

### Include a figure
here it is a templete to include a figure (for this example the orcid logo is used as figure)

<p align = "center">
<img src="./data/fig/orcid.png" alt="name" width="300"/>
</p>
<p align = "center">
<figcaption align = "center"><a>caption.</a></figcaption>
</p>



Don't forget to add references if specific papers are cited!
