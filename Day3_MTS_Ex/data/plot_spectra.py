import numpy as np
import matplotlib.pyplot as plt

wn_blyp, signal_blyp = np.loadtxt('./blyp_ref/SPECTRUM', unpack=True)
wn_b3lyp, signal_b3lyp = np.loadtxt('./b3lyp_ref/SPECTRUM', unpack=True)
wn_mts7, signal_mts7 = np.loadtxt('./1_b3lyp_7_blyp/SPECTRUM', unpack=True)

plt.xlabel(r'Wavenumber in $\mathrm{cm^-1}$')
plt.ylabel(r'Signal in arbitrary units ')
plt.xlim(0.0, 3000.0)
plt.plot(wn_blyp, signal_blyp, label=r'BLYP $\delta t = 20\ \mathrm{a.u.}$')
plt.plot(wn_b3lyp, signal_b3lyp+1, label=r'B3LYP $\delta t = 20\ \mathrm{a.u.}$')
plt.plot(wn_mts7, signal_mts7+2, label=r'MTS $\Delta t = 140\ \mathrm{a.u.}$')
plt.legend()
plt.show()

