import numpy as np

energies, timings = np.loadtxt('ENERGIES', usecols=[4, 7], unpack=True)

n = len(energies)
mean = np.mean(energies)
fluctuation = np.sum(np.abs((energies - np.ones(n) * mean) / mean)) / n

with open('FLUCTUATION', 'w') as out:
    out.write(f'{fluctuation}\n')

with open('TIMING', 'w') as out:
    out.write(f'{np.mean(timings)}\n')
