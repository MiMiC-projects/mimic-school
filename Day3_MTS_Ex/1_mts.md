# Multiple time step approach with acetone

With the Multiple Time Step (MTS) approach, we are able to simulate trajectories at a high level of accuracy with relatively low computational costs.
In this tutorial, you will learn how to:

1. Set up MTS BOMD simulations with MiMiC
2. Assess their quality and performance
3. Validate that MTS reproduces high-level dynamics

This tutorial builds on the exercises of Day 1 and is based on the following sources:

* [Elisa Liberatore, Rocco Meli, and Ursula Rothlisberger: A Versatile Multiple Time Step Scheme for Eﬃcient ab Initio Molecular Dynamics Simulations (2018)](https://pubs.acs.org/doi/10.1021/acs.jctc.7b01189)
* [Efrem Braun: Calc IR Spectra From Lammps (2016)](https://github.com/EfremBraun/calc-ir-spectra-from-lammps)

For this tutorial, we will assume that you cloned the github repository, hence you will have the provided folders and files in the same locations.

```
git clone https://gitlab.com/MiMiC-projects/mimic-school.git
```

This command will clone the `mimic-school` repository in the current folder. Navigate the repository and create your own directory to run this exercise:

```
cd mimic-school/Day3_MTS_Ex
mkdir solution
cd solution
```

## Set up a MTS simulation

We will start from the structure of Day 1 after the annealing and re-heating procedure followed by 10 ps of NVT equilibration.
You will need the following files:

* RESTART
* mimic.tpr
* mts.inp

<details>
<summary>:information_source: simulated annealing</summary>
<hr>   
As the annealing and re-heating procedure is just a way of getting the system to a point from which one can run a stable QM/MM molecular dynamics simulation, it is OK to carry out these steps on a lower level of theory.<hr>   
</details>

The `&CPMD` section in `mts.inp` should look similar to this:

```
&CPMD
    USE_MTS
    MOLECULAR DYNAMICS BO
    RESTART COORDINATES VELOCITIES WAVEFUNCTION
    ISOLATED MOLECULE
    NEW CONSTRAINTS
    EXTRAPOLATE WFN
        8
    TIMESTEP
        20.0
    MAXSTEP
        10000
    TRAJECTORY SAMPLE FORCES XYZ
        10
    DIPOLE DYNAMICS SAMPLE
        1
    STORE
        100
    RESTFILE
        1
    MIMIC
&END
```

Note the additional `USE_MTS` keyword. This allows to specify two different functionals in the `&DFT` section:

```
&DFT
    XC_DRIVER
    MTS_HIGH_FUNC HYB_GGA_XC_B3LYP
    MTS_LOW_FUNC GGA_XC_BLYP
&END
```

With BLYP and B3LYP we chose a lower-level GGA and a higher-level hybrid functional.
The ratio between high- and low level steps is set in the `&MTS` section:

```
&MTS
    TIMESTEP_FACTOR
        n
    PRINT_FORCES ON
&END
```

This is the main parameter we want to tune in order to achieve the best compromise between performance (higher values for `n`) and accuracy (lower `n`).
How to do this will be explained in the next section.

## Assess quality and performance

In the case of the NVE ensemble, one measure of the quality of a simulation is the energy fluctuation [[Liberatore2018]](https://pubs.acs.org/doi/10.1021/acs.jctc.7b01189)

```math
  \varepsilon = \frac{1}{N} \sum_{i=1}^{N}\left|{\frac{E_i-\overline{E}}{\overline{E}}}\right| 
```

We will now test this by running multiple NVE simulations with different values for the `TIMESTEP_FACTOR` keyword, i.e. the outer timestep, not changing the inner `TIMESTEP`:
<details>
<summary>:information_source: inner timestep</summary>
<hr>   
The value of the inner timestep has in fact little influence on the accuracy of the MTS scheme.
<hr>   
</details>

```
mkdir b3lyp_blyp_1
mkdir b3lyp_blyp_3
mkdir b3lyp_blyp_5
mkdir b3lyp_blyp_7

cp ../data/* b3lyp_blyp_1
cp ../data/* b3lyp_blyp_3
cp ../data/* b3lyp_blyp_5
cp ../data/* b3lyp_blyp_7
```

In each folder, adjust the value for `TIMESTEP_FACTOR` accordingly:

```
&MTS
    TIMESTEP_FACTOR
        1 *or* 3 *or* 5 *or* 7
    PRINT_FORCES ON
&END
```

:warning: **Update the path in the `PATH` keyword in the `&MIMIC` section** 

To get a frame of reference, you will additionally run two non-MTS Velocity-Verlet simulations, the first one with a `TIMESTEP` of 20 a.u. and the second one with 80 a.u.:

```
mkdir blyp_20au
mkdir blyp_80au

cp ../data/* blyp_20au
cp ../data/* blyp_80au

mv blyp_20au/mts.inp blyp_20au/vv.inp
mv blyp_80au/mts.inp blyp_80au/vv.inp
```

Remove the `USE_MTS` keyword in the `&CPMD` section and the `&MTS` section from `vv.inp`.
Also, adjust the `TIMESTEP` value (20 a.u. or 80 a.u.) and change the `&DFT` section back to this:

```
&DFT
    XC_DRIVER
    FUNCTIONAL GGA_XC_BLYP
&END
```

:warning: **Update the path in the `PATH` keyword in the `&MIMIC` section**

While your simulations are running, you can monitor the energies:

```
tail -f ENERGIES 
```

Once your simulations are completed, you should carefully check the results:

```
gnuplot
```
```
set multiplot layout 1, 2
set title "Temperature"
set xlabel "MD step"
set ylabel "Temperature (K)"
plot 'ENERGIES' u 1:3 w l lt 1 title "TEMPP"
set title "Physical Energy"
set xlabel "MD step"
set ylabel "Energy (a.u.)"
plot 'ENERGIES' u 1:5 w l lt 2 title "ECLASSIC"
```
```
quit
```

If everything looks OK, you can calculate the energy fluctuation and mean time per timestep for each run.
The increase in energy fluctuations should be smaller for the MTS runs than for the VV runs.
When you look at the MTS timings, you will notice that the curve flattens out:

<p align = "center">
<img src="./data/fig/timings.png" alt="timings" width="300"/>
</p>
<p align = "center">
<figcaption align = "center">Timings over timestep factor</figcaption>
</p>

This is because the initial guess for the high-level wavefunction, which is extrapolated from previous steps, gets worse the more low-level steps are in between.
As a result, there are more iterations needed to converge the wavefunction.

***Based on your results, what do you consider a good choice for the timestep factor?***
***What changes when you switch from NVE to the NVT ensemble?***

## Validate dynamical properties

Finally, we will show that MTS can repruduce results from high-level simulations.
For this, we will compare the vibrational spectra from the MTS runs (`TIMESTEP_FACTOR` 3, 5, or 7) to the B3LYP refernce (`TIMESTEP_FACTOR` 1) and the BLYP trajectory (`TIMESTEP` 20 a.u.).

:warning: **Change the timestep in the script to calculate the spectrum** 

```
cd blyp_20au
python3 vibrational_spectrum.py
cd ..

cd b3lyp_blyp_1
python3 vibrational_spectrum.py
cd ..
...
```

This will create a file `SPECTRUM` in each subfolder, that you can now combine in a sigle plot:

<p align = "center">
<img src="./data/fig/vs.png" alt="vs" width="300"/>
</p>
<p align = "center">
<figcaption align = "center">Vibrational spectra of acetone (QM) in water (MM). Due to time restrictions, the sampling is rather low.</figcaption>
</p>

The peak around $`1800\, \mathrm{cm^{-1}}`$ is red-shifted for the BLYP simualtion compared to the B3LYP refernce.
On the other hand, the MTS runs reproduce the higher-level spectrum.
