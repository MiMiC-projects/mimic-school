# MiMiC School

Tutorial for CECAM school: Multiscale Molecular Dynamics with MiMiC (July 18-22, 2022) at CECAM-HQ-EPFL, Lausanne, Switzerland ([Event page](https://www.cecam.org/workshop-details/1119 "CECAM school")).   


## Day 1 - Acetone 
Introductory tutorial to perform QM/MM simulations using MiMiC on a small system, composed of an acetone molecule in water.

:information_source: Tutorial based on:
* MiMiC tutorial from 2018 by Viacheslav Bolnykh [![Orcid](./Day1_Acetone_Ex/data/fig/orcid.png)](https://orcid.org/0000-0002-7090-1993) and Emiliano Ippoliti [![Orcid](./Day1_Acetone_Ex/data/fig/orcid.png)](https://orcid.org/0000-0001-5513-8056)
* Updates on the MiMiC tutorial (February 2021) and MiMiCPy documentation by Bharath Raghavan [![Orcid](./Day1_Acetone_Ex/data/fig/orcid.png)](https://orcid.org/0000-0003-0186-2625)
* CPMD tutorial for the BioExcel Summer School on Biomolecular Simulations 2018 in Pula (Italy) from [Bonvin Lab](https://www.bonvinlab.org/education/biomolecular-simulations-2018/CPMD_tutorial/).   

:information_source: Main contributors to the updated version of this tutorial:  
* Andrea Levy [![Orcid](./Day1_Acetone_Ex/data/fig/orcid.png)](https://orcid.org/0000-0003-1255-859X)
* Sophia Johnson [![Orcid](./Day1_Acetone_Ex/data/fig/orcid.png)](https://orcid.org/0000-0003-4207-4350)   
* Bharath Raghavan [![Orcid](./Day1_Acetone_Ex/data/fig/orcid.png)](https://orcid.org/0000-0003-0186-2625)
* Bharath Raghavan [![Orcid](./Day1_Acetone_Ex/data/fig/orcid.png)](https://orcid.org/0000-0003-0186-2625)
* Sonata Kvedaravičiūtė [![Orcid](./Day1_Acetone_Ex/data/fig/orcid.png)](https://orcid.org/0000-0001-8432-9142)
* David Carrasco de Busturia [![Orcid](./Day1_Acetone_Ex/data/fig/orcid.png)](https://orcid.org/0000-0003-1588-338X)


## Day 2 - GB1 enzyme 
Introductory tutorial to perform QM/MM simulations using MiMiC on a biologically relevant system (GB1 mutant). Efficient use of resources is also discussed.

:information_source: Tutorial based on:
* CPMD tutorial for the BioExcel Summer School on Biomolecular Simulations 2018 in Pula (Italy) from [Bonvin Lab](https://www.bonvinlab.org/education/biomolecular-simulations-2018/CPMD_tutorial/).   
* [Original MiMiC publication](https://doi.org/10.1021/acs.jctc.9b00093 "MiMiC paper"). 

:information_source: Main contributors to the updated version of this tutorial:  
* Andrea Levy [![Orcid](./Day1_Acetone_Ex/data/fig/orcid.png)](https://orcid.org/0000-0003-1255-859X)
* Sophia Johnson [![Orcid](./Day1_Acetone_Ex/data/fig/orcid.png)](https://orcid.org/0000-0003-4207-4350)   
* Andrej Antalík [![Orcid](./Day1_Acetone_Ex/data/fig/orcid.png)](https://orcid.org/0000-0002-8422-8410)
* Bharath Raghavan [![Orcid](./Day1_Acetone_Ex/data/fig/orcid.png)](https://orcid.org/0000-0003-0186-2625)

## Day 3 - Multiple Time Step Scheme

Tutorial to perform Multiple Time Step (MTS) QM/MM simulations using MiMiC starting from exercises of Day 1.

:information_source: Tutorial based on:
* [Elisa Liberatore, Rocco Meli, and Ursula Rothlisberger: A Versatile Multiple Time Step Scheme for Eﬃcient ab Initio Molecular Dynamics Simulations (2018)](https://pubs.acs.org/doi/10.1021/acs.jctc.7b01189)
* [Efrem Braun: Calc IR Spectra From Lammps (2016)](https://github.com/EfremBraun/calc-ir-spectra-from-lammps) 

:information_source: Main contributors are:  
* Andrej Antalík [![Orcid](./Day1_Acetone_Ex/data/fig/orcid.png)](https://orcid.org/0000-0002-8422-8410)
* Florian Schackert [![Orcid](./Day1_Acetone_Ex/data/fig/orcid.png)]( https://orcid.org/0000-0001-6028-3717)

## Day 4 - 
Tutorial to perform thermodynamic intergation using MiMiC and PLUMED, to find the free energy profile of the proton transfer between Asp107 and Glu105 in β-glucanase.

:information_source: Tutorial based on:
* Exercise 2 and exercise 4 from the CECAM Summer School ["Hybrid Quantum Mechanics / Molecular Mechanics (QM/MM) Approaches to Biochemistry and Beyond"](https://www.cecam.org/workshop-details/1030) - May 2022, Lausanne (Switzerland) by M. Boero[![Orcid](./Day1_Acetone_Ex/data/fig/orcid.png)](https://orcid.org/0000-0002-5052-2849), P. Campomanes[![Orcid](./Day1_Acetone_Ex/data/fig/orcid.png)](https://orcid.org/0000-0001-9229-8323),  C. Rovira[![Orcid](./Day1_Acetone_Ex/data/fig/orcid.png)](https://orcid.org/0000-0003-1477-5010), and A. P. Seitsonen
* Discussion on thermodynamic integration with Simone Meloni [![Orcid](./Day1_Acetone_Ex/data/fig/orcid.png)](https://orcid.org/0000-0002-3925-3799)
* Testing of the tutorial by Sonata Kvedaraviciute [![Orcid](./Day1_Acetone_Ex/data/fig/orcid.png)](https://orcid.org/0000-0001-8432-9142)

:information_source: Main contributors to this tutorial:   
* Bharath Raghavan [![Orcid](./Day1_Acetone_Ex/data/fig/orcid.png)](https://orcid.org/0000-0003-0186-2625)

