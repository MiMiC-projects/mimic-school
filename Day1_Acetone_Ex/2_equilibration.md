# Equilibration with Classical MD 
We are interested in studying the effects of the presence of water molecules on the dipole of acetone molecule, but the solvated system that we just created is based on the structure of acetone in vacuum.  Hence, we need to equilibrate the system before continuing with QM/MM MD. In any case, the first step before running a QM/MM simulation is a classical force field-based molecular dynamics equilibration. 

The equilibration we will perform is composed of two main steps:
1. Energy minimization on the initial structure which is needed to minimize the energy with the selected force field of the starting strucutre of the solvated system. This step will, for example, improve the orientation of hydrogen bonds. 
2. Equilibration during which we will heat the system, using the minimized structure, by increasing temperature from 0K to 300K (room temperature).
 
## Energy Minimization
Create a folder where we are going to perform the energy minimization step:

```
mkdir  ../3_mini
cd ../3_mini
```

The parameters for the minimization are set in the provided `mini.mdp` file. Copy the file in the current directory and some of the files that we have previously generated, which will be the starting point for the minimization:

```
cp ../../data/mini.mdp .
cp ../2_system_prep/solvated.gro .
cp ../2_system_prep/acetone.top .
cp ../2_system_prep/acetone.itp .
```

<details>
<summary>:information_source: mini.mdp file</summary>
<hr>   
 
 `.mdp` files contain parameters to set up a molecular dynamics run with a given system. We later process this file with the `grompp` command (explained later) to generate a binary `.tpr` file. The `.mdp` file parameters depend on the specific process and system so each time we move to a different MD process we'll need a new `.mdp` input file (for example when we transition to QM/MM). Below we provide a sample `.mdp` file for an energy minimization run with comments following `;` symbols:

```
    ; General MD parameters for an energy minimization process
    integrator    = steep    ; MD Algorithm selection (steep = steepest descent minimization). 
    emtol         = 100.0    ; stop minimization when the maximum force < 100.0 kJ/mol/nm
    emstep        = 0.01     ; energy step size
    nsteps        = 50000    ; maximum number of (minimization) steps to perform
    
    ; Parameters describing how to find the neighbors of each atom and how to calculate their interactions
    nstlist        = 1       ; frequency to update the neighbor list and long range forces. Lowest possible value is 0 which equates to a list that is created and then never updated
    cutoff-scheme  = Verlet  ; type of cut-off scheme. `Verlet` generates a pair list with buffering
    ns_type        = grid    ; method to determine neighbor list (simple, grid)
    coulombtype    = PME     ; treatment of long range electrostatic interactions. This example shows a Particle-Mesh Ewald selection (`PME`). For more options see GROMACS documentation link below.
    rcoulomb       = 1.0     ; short-range electrostatic cut-off in nm
    rvdw           = 1.0     ; short-range Van der Waals cut-off in nm
    pbc            = xyz     ; periodic boundary conditions used in all directions (`xyz`). Other options could be `no` to ignore the box or `xy` to apply pbc to x and y directions only.
```

For more details see [GROMACS documentation](https://manual.gromacs.org/current/user-guide/mdp-options.html).
<hr>   
</details>

The `.mdp` files can be converted into binary files using the GROMACS preprocessor command `grompp`, which requires different inputs (recall the use of `gmx_mpi_d` in lieu of `gmx` if you're using MPI in your commands). Prior to running inspect the acetone.top file oo ensure that `ACT` and `SOL` are on separate lines in `molecules` section of the file with values `1` and `506` under `#mols` for the two molecule types respectively:

```
gmx grompp -f mini.mdp -c solvated.gro -p acetone.top -o mini.tpr
```

<details>
<summary>:information_source: grompp command</summary>
<hr> 
 
The `grompp` command converts our `solvated.gro` file from a molecular topology file into an atomic description binary file, `mini.tpr`, according to the parameters specified in the `mini.mdp` input file. Different options can be used:

* `-f` read the MD parameter inputs from the `.mdp` file
* `-c` structure file in `.gro` or `.g96` or `.pdb` or `.brk` or `.ent` or `.esp` or `.tpr`
* `-p` topology file in `.top` format
* `-n` [optional] index file in `.ndx` format
* `-r` or `-rb` [optional] restraints in any of the formats possible with the `-c` flag, could even be the same file as provided for the `-c` flag
* `-t` [optional] trajectory in `.trr` or `.cpt` or `.tng` format to read starting coordinates if needing to restart a process
* `-e` [optional] energy file in `.edr` format to provide Nose-Hoover and/or Parrinello-Rahman coupling variables 
 
 Additionally, `grompp` uses a built-in preprocessor which can support the following keywords:
```
#ifdef VARIABLE
#ifndef VARIABLE
#else
#endif
#define VARIABLE
#undef VARIABLE
#include "filename"
#include <filename>
```
And these keywords can be enacted and changed in the associated `.mdp` file by including statements like:
```
define = -DVARIABLE1 -DVARIABLE2
include = -<file_path>
```
There are additional options to control the output files. For more details see [GROMACS documentation](https://manual.gromacs.org/current/onlinehelp/gmx-grompp.html).
<hr> 
</details>

Now that we generated the `mini.tpr` file, we are actually able to run the minimization using the `mdrun` command. In particular we will run in parallel on 4 cores (`-ntomp 4`) by taking advantage of the OpenMP threads. 

```
gmx mdrun -deffnm mini -ntomp 4 &
```
<details>
<summary>:information_source: mdrun command</summary>
<hr> 
The `mdrun` launches molecular dynamics runs in GROMACS. Many different options can be used and just a few are listed below:

* `-deffnm` uses the following string for all the file options (input, output, error, log, etc)
* `-s` portable xdr run input file in`.tpr` format
* `-o` output a full precisoin trajectory in a `.trr` or `.cpt` or `.tng` format
* `-c` structure file in `.gro` or `.g96` or `.pdb` format
* `-ntomp` number of OpenMP threads per MPI rank
 
For more details see [GROMACS documentation](https://manual.gromacs.org/current/onlinehelp/gmx-mdrun.html).
<hr> 
</details>

This prodecure will generate different output files. In particular, the progress of the minization can be monitored using the `.log` file, using the command 

```
tail -f mini.log
```

When the minimization will be completed, a mesage will be printed in the `log` file, stating how many steps have been required to converge the minimization, together with other interesting quantities to look at, such as the potential energy of the final configuration and the maximum force (and the atom on which it acts).
 The final configuration can be found in the `mini.gro` file, and will become the starting point for the heating.

<!--
N.B. The files provided should work for the tutorial. However, please be aware that GROMACS needs the atom type input to be in the 5th column exactly of any gro file you provide. If you receive an error from GROMACS stating that your atotm types are not correct or not in the correct order, please retrun to your gro files and verify that your cursor is at column 14 just to the left of the atom type. Additionally, verify that the atom names in your gro files match those in your itp file exactly. For example if you have an O3 atom in your solvated.gro file, it should match the same coordinnates and name in the itp file.  
-->

## Heating - Classical Equilibration
The next step will be to thermalize the system classically and to adjust the pressure in the box to reach standard conditions. Create a folder where we are going to perform the heating step and copy there some of the files we have generated, together with the input `heat.mdp` file (provided). We need a new `.mdp` file because we are are performing a different type of MD simulation and therefore need different MD parameters. 

```
mkdir  ../4_heat
cd ../4_heat
cp ../../data/heat.mdp .
cp ../3_mini/mini.gro .
cp ../3_mini/acetone.top .
cp ../3_mini/acetone.itp .
```

<details>
<summary>:information_source: heat.mdp file</summary>
<hr> 

The `heat.mdp` requires information about the temperature and pressure control. You'll see that we need to define more parameters than we did for the simple energy minimization process. Below we provide a sample `.mdp` file for a classical equilibration run with comments following `;` symbols:
 
```    
; Run parameters
integrator      = md          ; MD Algorithm selection (leap-frog integrator)
dt              = 0.002       ; time step in ps. Here we define a 2 fs time step
nsteps          = 100000      ; number of steps in the simulation. Here: 2 fs/step * 100000 steps = 200 ps

; Output control
nstxout         = 500         ; save coordinates every 1.0 ps
nstvout         = 500         ; save velocities every 1.0 ps
nstenergy       = 500         ; save energies every 1.0 ps
nstlog          = 500         ; update log file every 1.0 ps

; Bond parameters
continuation    = no          ; when turned on, this option specifies NOT to apply constraints to start or to reset shells. 
                              ; This is useful to switch to `yes` for a re-run or an exact continuation. 
                              ; Since this is our first dynamics run in the process we have this option set to `no` so that the constraints are applied at the start of the run. 
constraint_algorithm  = lincs ; choice of constraint solver for any non-SETTLE holonomic constraints. Here we select a LINear Constraint Solver (lincs).
constraints     = all-bonds   ; which bonds are constrainted. `all bonds` means all bonds convert to constraints (even heavy atom-H bonds).
lincs_iter      = 1           ; max number of iterations for the lincs solver which controls the accuracy of LINCS
lincs_order     = 4           ; number of matrices in the expansion for the matrix inversion during constraint solving. This is also related to accuracy

; Neighbor searching
cutoff-scheme   = Verlet      ; type of cut-off scheme. `Verlet` generates a pair list with buffering
ns_type         = grid        ; method to determine neighbor list (simple, grid)
nstlist         = 10          ; frequency to update the neighbor list and long range forces. Lowest possible value is 0 which equates to a list that i is created and then never updated. This option is largely irrelevant with a Verlet cut-off scheme.
rcoulomb        = 1.2         ; short-range electrostatic cutoff in nm
rvdw            = 1.2         ; short-range van der Waals cutoff in nm

; Electrostatics
coulombtype     = PME         ; treatment of long range electrostatic interactions. This example shows a Particle-Mesh Ewald selection (`PME`). For more options see GROMACS documentation link below.
pme_order       = 4           ; order of PME interpolation. Here we define a cubic interpolation. 
fourierspacing  = 0.16        ; grid spacing for FFT in nm. For optimizing the relative load of the particle-particle interactions and the mesh part of PME, it is useful to know that the accuracy of the electrostatics remains nearly constant when the Coulomb cut-off and the PME grid spacing are scaled by the same factor.

; Temperature coupling
tcoupl          = V-rescale   ; selection of thermostat/temperature control scheme. Here we select a modified Berendsen thermostat.
tc-grps         = System      ; define groups to couple to separate thermal baths. Here, with two coupling groups for system and solvent (basically everything else except the system) we can run a more accurate/efficient equilibration process.
tau_t           = 0.1         ; time constant for coupling with the bath in ps. A value of -1 specifies no coupling.
ref_t           = 300         ; reference temperature, one for each group defined, in K

; Pressure coupling
pcoupl          = Berendsen   ; selection of barostat/pressure control scheme. Here we select a modified Berendsen barostatting scheme.
pcoupltype      = isotropic   ; type or isotropy of pressure coupling. Here we employ a uniform scaling of box vectors with tau-p. When this option is selected a compressibility value and reference pressure value are required. 
tau_p           = 2.0         ; time constant for pressure coupling, in ps
ref_p           = 1.0         ; reference pressure, in bar
compressibility = 4.5e-5      ; isothermal compressibility of water at your target conditions, bar^-1

; Periodic boundary conditions
pbc             = xyz         ; periodic boundary conditions used in all directions (`xyz`). Other options could be `no` to ignore the box or `xy` to apply pbc to x and y directions only.

; Dispersion correction
DispCorr        = EnerPres    ; dispersion corrections to account for the cut-off applied for vdw. Here `EnerPres` tells our system to apply long range disperssion corrections for both Energy and Pressure outputs. 
   
; Velocity generation
gen_vel         = yes         ; generate velocities with a random seed according to a Maxwell distribution
gen_temp        = 300         ; temperature for the Maxwell distribution
gen_seed        = -1          ; generate a random seed. A `-1` value uses a pseudo random seed
```
 
For more details see [GROMACS documentation](https://manual.gromacs.org/current/user-guide/mdp-options.html).
<hr> 
</details>

As before, first create the `.tpr` binary file  using the GROMACS preprocessor command `grompp`

```
gmx grompp -f heat.mdp  -c mini.gro -p acetone.top -o heat.tpr
```

and then run the equilibration simulation:
```
gmx mdrun -deffnm heat -ntomp 2 &
```

As before, the progress of the heating phase can be monitored using the `tail` command

```
tail -f heat.log
```
and at the end a report will be printed to the file, and a `heat.gro` file containing the equilibrated configuration will be generated.

<details>
<summary>:information_source: Selecting a thermostat</summary>
<hr> 
 
GROMACS provides several options for temperature control during a molecular dynamics simulation. You can add the option in the `.mdp` file with the keyword `tcoupl' followed by one of the options below:
* `-no` no temperature control
* `-nose-hoover` the Nose-Hoover thermostat defines an extended Lagrangian to manage the thermostat variables. Nose-Hoover chains are the typical choice for NVE production runs due to their stabiliy. 
* `-berendsen` the Berendsen thermostat adds a small coupling factor which moves the system towards a reference temperature.  This is a "historical" thermostat mainly present to be able to reproduce previous simulations, but it is strongly recommend not to use it for new production runs
* `-andersen` the Andersen thermostat randomizes velocities of some particles at each time step 
* `-andersen-massive` the Andersen massive thermostat randomizes velocities of some particles but not at each time step
* `-v-rescale` temperature coupling using velocity rescaling with a stochastic term. This thermostat is similar to Berendsen coupling, but the stochastic term ensures that a proper canonical ensemble is generated. 

We must define other thermostat variables in the `.mdp` file such as reference temperature (`ref-t`) and time constant for coupling (`tau-t`). For more details see [GROMACS documentation:](https://manual.gromacs.org/5.1.1/user-guide/mdp-options.html?#temperature-coupling). 

<hr> 
</details>

 
### Equilibration Check
Is the system well equilibrated? This can be assessed using the GROMACS `energy` tool, to monitor the behavior along the trajectory of the quantities you are interested in. Select the quantities you want to check using the corresponding number, and type a `0` to exit the command:

```
gmx energy -f heat.edr -o heat_check.xvg
```
for example, to look at total energy (11) and temperature (13), type:
```
> 11 13 0
```
This will perform a statistical analysis over all the steps stored in the `.edr` file and plot in particular the average and the estimated error on the terminal. Note that because the `energy` command is interactive with the user, you should perform it directly in command line if you have been previously using scripts. Just make sure any necessary modules and GROMACS sources and paths have been loaded into your coding environment before executing. You should obtain something similar to:
```
Statistics over 100001 steps [ 0.0000 through 200.0000 ps ], 2 data sets
All statistics are over 1001 points

Energy                      Average   Err.Est.       RMSD  Tot-Drift
-------------------------------------------------------------------------------
Total Energy               -17521.7        6.8    222.898   0.964539  (kJ/mol)
Temperature                 299.609       0.41    8.01493   0.616571  (K)
```
Check that the average temperature is close to 300K, or in general to the temperature you set as `ref-t`, without too large of fluctuations. 

Note that the above command has also generated a `heat_check.xvg` file, which is helpful to plot and then visually inspect data. This file is organized in columns based on the quantities selected before. The first column contains the time in ps (we asked GROMACS to store the energy every 500 steps, i.e. 1 ps using a timestep of 0.002 ps, setting `nstenergy 500` in the `heat.mdp` file).


You can use any program to visualize this, in particular using `gnuplot` the commands needed to plot `Total-Energy` (column `2`) and `Temperature` (column `3`), are:
```
gnuplot
```
```
set multiplot layout 1, 2
set title "Total Energy"
set xlabel "Time (ps)"
set ylabel "Energy (kJ/mol)"
plot 'heat_check.xvg' u 1:2 w l lt 2 title "Total Energy"
set title "Temperature"
set xlabel "Time (ps)"
set ylabel "Temperature (K)"
plot 'heat_check.xvg' u 1:3 w l lt 1 title "Temperature"

```
```
quit
```
Here you can see an example of the evolution of these two quantities during the heating phase:

<p align = "center">
<img src="./data/fig/heating_check.png" alt="heating_check" width="600"/>
</p>
<p align = "center">
<figcaption align = "center"><a>Evolution of total energy and temperature along the heating (classical MD).</a></figcaption>
</p>

Both the energy and the temperature should oscillate around fixed values, in particular equal to 300K (`ref-t`) for the temperature. Thesetwo plots, together with the average values and the errors estimated with the `energy` tool, confirm that the system is well equilibrated. We are now finally ready for QM/MM MD!
