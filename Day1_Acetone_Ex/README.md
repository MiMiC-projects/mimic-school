# Acetone Tutorial

Introductory tutorial to perform QM/MM simulations using MiMiC on a small system, composed of an acetone molecule in water.

Main steps:
1. Calculate the dipole moment of acetone in vacuum
2. Solvate the system and equilibrate it with classical MD
3. Prepare the input for MiMiC and perform a QM/MM MD simulation at room temperature
4. Calculate the average dipole moment from selected snapshots of the production run
5. Estimate the difference between the dipole moment in vacuum and the one at room temperature to understand the effects of solvent and temperature 

Theoretical prerequisites for this tutorial: 
* Density functional theory (DFT)
* Basis set (localized and plane-waves) 
* Pseudopotentials
* Classical and quantum molecular dynamics (QM/MM)
* Classical force field based molecular dynamics  

:warning: This tutorial shows how to run a QM/MM MD simulation with MiMiC from scratch, including the equilibration of the system with classical MD. If you are already familiar with these preliminary steps, you can direcly start from Section 3 (MiMiC). All the files generated during these preliminary steps useful to start a QM/MM simulation are provided in the `Day1_Acetone_Ex/data/` folder.

:warning: This tutorial assumes the user to have running versions of MiMiC, CPMD and GROMACS. A a python virtual environment to run the `mimicpy` code is also assumed. Instructions to create a python environment can be found in  `mimic-school/mimicpy_setup.md`.

:warning: To run the tutorial clone the git repository in your local machine. Input files needed for this tutorial are located in the `mimic-school/Day1_Acetone_Ex/data/` folder.

:information_source: Tutorial based on:
* MiMiC tutorial from 2018 by Viacheslav Bolnykh [![Orcid](./data/fig/orcid.png)](https://orcid.org/0000-0002-7090-1993) and Emiliano Ippoliti [![Orcid](./data/fig/orcid.png)](https://orcid.org/0000-0001-5513-8056)
* Updates on the MiMiC tutorial (February 2021) and MiMiCPy documentation by Bharath Raghavan [![Orcid](./data/fig/orcid.png)](https://orcid.org/0000-0003-0186-2625)
* CPMD tutorial for the BioExcel Summer School on Biomolecular Simulations 2018 in Pula (Italy) from [Bonvin Lab](https://www.bonvinlab.org/education/biomolecular-simulations-2018/CPMD_tutorial/).   

:information_source: Main contributors to the updated version of this tutorial:  
* Andrea Levy [![Orcid](./data/fig/orcid.png)](https://orcid.org/0000-0003-1255-859X)
* Sophia Johnson [![Orcid](./data/fig/orcid.png)](https://orcid.org/0000-0003-4207-4350)   
* Bharath Raghavan [![Orcid](./data/fig/orcid.png)](https://orcid.org/0000-0003-0186-2625)
* Sonata Kvedaravičiūtė [![Orcid](./data/fig/orcid.png)](https://orcid.org/0000-0001-8432-9142)
* David Carrasco de Busturia [![Orcid](./data/fig/orcid.png)](https://orcid.org/0000-0003-1588-338X)

