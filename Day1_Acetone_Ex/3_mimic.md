# MiMiC
Now that the preliminary steps are completed, and the system has been equilibrated at classical level, we are finally ready to run QM/MM MD!

:warning:  All the files generated during these preliminary steps useful to start a QM/MM simulation are provided in the `Day1_Acetone_Ex/data/` folder.

## Input Preparation with MiMiCPy
MiMiC uses CPMD together with GROMACS in order to run a QM/MM simulation. The CPMD input file and the GROMACS `.tpr` binary file have to be prepared at the same time, making sure that both CPMD and GROMACS receive the same QM atoms information. This can be easily achieved with the MiMiCPy package (as explained in the introduction, a python virtual environment to run the `mimicpy` code is assumed. Instructions to create a python environment can be found in `mimic-school/mimicpy_setup.md`).
```
mkdir ../5_mimic_prep
cd ../5_mimic_prep
```

If you followed the entire tutorial, you should have produced all the files needed. In any case, the relevant files are also provided in the `Day1_Acetone_Ex/data/` folder.
Either copy your files in the folder just created, or the ones provided
```
cp ../../data/acetone_equilibrated.gro ./heat.gro
cp ../../data/acetone_equilibrated.top ./acetone.top
cp ../../data/acetone.itp .
```

### Index file 
<!---
Previous instructions, in the current version uncommented made specific for CECAM school on G100
:warning: Before running the following command, make sure that the GROMACS has been sourced, i.e. available on the command line. This is because MiMiCPy requires the GMXDATA environment variable to be set, so that it can access the force field folder in the GROMACS installation. 
```
conda activate mimicpy
mimicpy prepqm -top acetone.top -coords heat.gro 
```
--->
To be able to use MiMiCPy we set up a python virtual environment which needs to be activated and first we need to source and prepare the code environment for the version of GROMACS we'd like to use (*specific to MiMiC school in 2022 using Cineca's G100 cluster*):
```
module purge
module load intel-oneapi-compilers/2021.4.0
module load intel-oneapi-mkl/2021.4.0
module load intel-oneapi-mpi/2021.4.0
module load python/3.8.12--intel--2021.4.0
source /g100_work/tra22_qmmm/Programs/gromacs-2021.5/bin/GMXRC.bash
export GMX_PATH=/g100_work/tra22_qmmm/Programs/gromacs-2021.5/bin/
```
```
. /g100_work/tra22_qmmm/Programs/mimicpy_venv/bin/activate
export GMXLIB='/g100_work/tra22_qmmm/Programs/gromacs-2021.5/share/gromacs' 
```
Now you can run the MiMiCPy `prepqm` script 
```
mimicpy prepqm -top acetone.top -coords heat.gro 
```
The command will open an interactive session with the following message printed out:
```
Some atom types had no atom number information.
They were guessed as follows:

+---------------------+
| Atom Type | Element |
+---------------------+
|     c     |    C    |
+---------------------+
|     c3    |    C    |
+---------------------+
|     o     |    O    |
+---------------------+
|     hc    |    H    |
+---------------------+
```
MiMiCPy requires the atomic element information for each atom to generate the CPMD input script. Some non-standard atom types in the GROMACS topology file do not contain this information. MiMiCPy guesses them based on the combination of atomic mass, name and type.  

:warning: Always verify that the guessed atomic elements for each atom type are meaningful, as it is essential for CPMD to run correctly.

<details>
<summary>:information_source: Handling non standard atomtypes with MiMiCPy</summary> 
<hr>
  
MiMiCPy guesses non-standard atom types based on the combination of atomic mass, name and type. The automatic behavior of guessing atomic elements can be toggled on or off using the `-guess` option. If `False` is passed and non-standard atoms are present, MiMiCPy will exit with an error message instead of attempting to guess the elements.  If you are not satisfied with the element information guessed, a file containing the list of all non-standard atom types with the correct element information can be passed to `mimicpy prepqm` with the `-nsa` option. For example, if MiMiCPy guessed the wrong atomic number for atom type `c3` and `o`, we could create a file called `atomtypes.dat` with the following text:
```
o O
c3 C
```
and pass it to MiMiCPy when running `prepqm`
```
mimicpy prepqm -top acetone.top -coords heat.gro -nsa atomtypes.dat
```
<hr>                                                                                                                                                 </details>
  

At this point, the user can provide instructions to add and/or deleted atoms to the QM region in an interactive session. The instructions specified after `add` correspond to the selection query that identifies the atoms to be added to the QM region. MiMiCPy provides a selection language similar to PyMOL and VMD for selecting QM atoms. Add the `ACT` molecule to the QM region with the following command:
```
> add resname is ACT
> q
```

If you'd prefer same instruction to `add resname is ACT` can be stored as a file `sele.dat` and passed to `mimicpy prepqm` for faster execution following these commands below (not necessary now that you've alrready completed the `prepqm` step):
```
cp ../../data/sele.dat .
mimicpy prepqm -top acetone.top -coords heat.gro -sele sele.dat
```


<details>
<summary>:information_source: Selection query in MiMiCPy command</summary> 
<hr>
  
The syntax for the selection query involves the following general structure:
  ```
  keyword logical_operator value
  ```
  where `keyword` can include `resname`, `resid`, `name`, `type`, `id`, and `mol`. `logical_operator` can be `is`, `not`, `>`, `<`, `>=`, or `<=`. 
  
  Different selection queries can be joined by using `and` or `or` operators, and grouped with brackets, such as
  ```
  add (resname is RES) and (name is x)
  ```
  which will add atom named `X` of residue `RES`. 
  With the same selection syntax atoms can also be deleted from the QM region, using the `delete` command instead of `add`.

  To check the current QM region, the `view` command can be used.  When the desired QM atoms are selected, type `q` to exit.
<hr> 
</details>
  
MiMiCPy generates the CPMD input script `cpmd.inp` and a GROMACS index file `index.ndx`. The latter has to be used to generate the GROMACS `.tpr` file needed for the QM/MM simulation with MiMiC. 

### Gromacs tpr file
A binary `.tpr` file can be generated using GROMACS preprocessor (`gmx grompp`), analogoulsy to what was done in the previous Section (Equilibration with Classical MD). Copy the `.mdp` file provided and run GROMACS preprocessor, passing the index file just generated. It is important that the label for QMMM-grps matches the index file label (here `qmatoms`)!
```
cp ../../data/mimic.mdp .
gmx_mpi_d grompp -f mimic.mdp  -c heat.gro  -p acetone.top -o mimic.tpr -n index.ndx 
```
The same coordinate and topology file used to run the earlier mimicpy command should be passed to gromacs grompp as well. 

If you have a `sele.dat` file prepared, the `.mdp` file is passed to `mimicpy prepqm` command we ran in the previous section and it can automatically generate the `.tpr` file (no need to do this now if you alerady generated the `.tpr` file directly with `grompp` command!):
```
mimicpy prepqm -top acetone.top -coords heat.gro -sele sele.dat -mdp mimic.mdp -gmx gmx_mpi_d -tpr mimic.tpr
```
With this command, MiMiCPy would generate not only the `cpmd.inp` and `index.ndx` files as before, but also `mimic.tpr` using `gmx grompp`, like the GROMACS command we just ran. Note that in order to excute this mimipy command you will need to have correctly sourced GROMACS and loaded all necessary modules as his command includes a call to GROMACS to perform the `grompp` command.

### CPMD input
Together with the `index.ndx` and/or `mimic.tpr` file, MiMiCPy generated also the CPMD input script `cpmd.inp`. This is a barebones CPMD input file, containing the most essential commands to run a MiMiC simulation.  
  
<details>
<summary>:information_source: :exclamation: MiMiC input for CPMD command</summary> 
<hr>   
  
A CPMD input file for a QM/MM simulation is similar to the CPMD input file for a standard QM calculation that has been previously described (Introduction section: Dipole Moment in Vacuum with CPMD). However, there are some main differences that should always be taken into account for the new QM/MM interface of CPMD:
  * In the `&CPMD` section the keyword `MIMIC` needs to be added
  * A new `&MIMIC` section, is mandatory. This section is the output from the MiMiCPy preprocessor, the 3 required keywords `PATHS`, `BOX` and `OVERLAPS` are provided. In this section, also other keywords can be added. The most relevant keywords for the `&MIMIC` section are:
    * `PATHS`, the number of the additional layers beyond the QM one treated by CPMD. In the case of QM/MM, this is `1`. In the following lines, for each layer the absolute path where the corresponding files are stored has to be specified (in our case the path where the GROMACS `.tpr` file is located).  
    :warning: **Always check that this path is consistent and is not indented - otherwise the simulation will get crash!**  
    * `BOX`, the size of the classical box, in a.u.
    * `OVERLAPS`, i.e. how many (first line) and which are the atoms (following lines) from one of the codes that should be treated by another one. In our case in this section the QM atom needs to be specified. The format for the lines specifying the overlapping atoms is a sequence of four numbers representing: the code (CPMD = 1,  GROMACS = 2), the atom ID, the cpde ID that has to treat it, and the atom ID in that code.
    * `LONG-RANGE COUPLING`, which turns on (if present) long-ranged electrostatic coupling to minimize the computational cost of the simulation. Interactions with MM atoms within `CUTOFF DISTANCE` (`20` in this case) will be treated using explicit Coulomb potential. For atoms that are outside the cutoff, a multipolar expansion of the electrostatic potential of an electronic grid will be used.
    * `FRAGMENT SORTING ATOM-WISE UPDATE`, which specifies the sorting frequency, i.e. the number of steps between two consecutive sorting of the atoms into the short and the long-ranged groups (`100` in this case).
    * `MULTIPOLE ORDER`, which dictates the order at which the multipolar expansion is trucated (`3` in this case).
  * In the `&ATOMS` section, the QM atoms has to be specified as in the full QM calculations. **Pseudopotential information, including the LMAX values, need to be adapted.**
  * The keyword `ANGSTROM` in the `&SYSTEM` section cannot be used. Lengths have to be expressed in a.u.
  * The option `ABSOLUTE` in the keyword `CELL` cannot be used. The correct syntax for the size of an orthorhombic box A x B x C is `A  B/A  C/A  0  0  0`
  * The QM system in a QM/MM calculation can only be dealt as isolated system, i.e. without explicit PBC since there is the MM environment all round it. Even though the option `ISOLATED SYSTEM` (or `0`) is used for the  `SYMMETRY` keyword, the calculation is, in fact, still done in a periodic cell: we are still using a plane wave basis set to expand the wavefunction of the QM part. Since the acetone molecule has a dipole moment, we have to take care of the long-range interactions between periodic images and there are methods (activated with the keyword `POISSON SOLVER` in the `&SYSTEM` section) implemented in CPMD to compensate for this effect. In particular, `TUCKERMAN` Poisson solver is used, since it has been proven to be the most effective one with typical systems studied in biology. Decoupling of the electrostatic images in the Poisson solver requires increasing the box size over the dimension of the molecule: practical experience shows that 3-3.5 Å between the outmost atoms and the box walls is usually sufficient for typical biological systems.

<hr>   
</details>

Details of the CPMD input to perform QM/MM calculations with MiMiC are described in the above drop-down menu. In particular, few things need to be noted/fixed:
  * The path specified in the `PATHS` keyword of the `&MIMIC` section refers to the path of the directory from which GROMACS will be run, i.e. where the `mimic.tpr` file is located. By default, MiMiCPy uses the current directory. Alternatively, a path can be specified with the `-path` option when running `mimicpy prepqm`.  
    :warning: **Always check that this path is consistent and is not indented - otherwise the simulation will get crash!**
  * The size of the QM region is specified in the `CELL` keyword in the`&SYSTEM` section. The cell size cannot be written in the `ABSOLUTE` format, and the correct syntax for the size of an orthorhombic box `A x B x C` is
```CELL   A       B/A     C/A     0       0       0```
where `0 0 0` refer to the cosines of the angles between the vector, i.e. perpendicular vectors in this case. By default, MiMiCPy writes a cell size exactly bounding the QM region,  often not enough to contain the plane waves of the QM region. For this reason, a padding will need to be added. This can be done by specifying a `-pad` when running `mimicpy prepqm`, i.e. an additional distance between the QM atoms and the walls of the QM box (in nm).  
A standard value for the Poisson solver is 3-3.5 Å , but in general the question of the right padding should be considered and chosen wisely.
  * The charge of the QM region is specified in the `CHARGE` keyword in the`&SYSTEM` section. This is calculated by MiMiCPy by summing the MM charges read from the force field data. This is usually enough, but it may need to be cross checked. Alternatively, a charge value to be written in the output can be specified using the `-q` option when running `mimicpy prepqm`.  
  * The QM atoms are specified in the`&ATOMS` section. Default values are written by MiMiCPy, but pseudopotential information needs to be adapted. This can be done by hand or passed a file `pp_info.dat` (provided) to the `-pp` option of mimicpy prepqm. This file should contains information like the pseuodopotential filenames for a specific atom, the same for the link atom version and other labels like LMAX and LOC.
    
    :warning: **Keep the `LMAX` values consistent with what we used for the dipole moment calculation in vacuum, namely `S` for `H` and `P` for `C` and `O`. By default, all these have been set to `S` by the preprocessor.**
  
Other options can be specified, they can be listed by typing `mimicpy prepqm --help`.
  
The extra parameters not in `cpmd.inp` file outputted by MiMiCPy can now be added in by hand. A more efficient way is to store all the extra parameters in a separate template file `template.inp` (provided). These are the parameters required for a full MiMiC simulated annealing (excluding the parameters in `&ATOMS` and `&MIMIC` sections outputted by mimicpy prepqm). MiMiCPy can be invoked again, but this time passing the `template.inp` file via the `-inp` option to provide a template for the generation of the complete input file. We will also use the `-pad` option, to properly define the size of the QM region.  We request a padding of 0.35 nm (3.5 Å) between the outermost QM atoms and the QM box wall to satisfy the Poisson solver's requirements. MiMiCPy adds this on both sides in each direction; therefore, a value of 0.7 nm is actually added in each direction to the size of the QM system. Follow the same procedure as before for the selection of the QM atoms in the interactive session or pass the `sele.dat` file directly, copyiing itt into your diirecttoory if you haven't already:
```
cp ../../data/sele.dat .
cp ../../data/template.inp .
mimicpy prepqm -top acetone.top -coords heat.gro -sele sele.dat -inp template.inp -pad 0.35 -out annealing.inp
```
This time MiMiCPy inserts the `&ATOMS` section, etc. into `template.inp`, generating the `annealing.inp` CPMD input file (whose name has been specified with the option `-out`). This is a full CPMD input file ready to be used for running MiMiC.

## QM/MM calculations
### Annealing
In the previous steps we obtained the equilibrated coordinates at room conditions using classical MD. This will be the starting point for a QM/MM simulation. However, we first need to equilibrate the system at QM/MM level since the two levels of theory are different. Therefore, it is necessary to firstly optimize the geometry of the system. A minimal energy structure at QM/MM level can be obtained with a simulated annealing (this is the reason of the keyword `ANNEALING IONS` in the input file), i.e. we run a Born-Oppenheimer MD where gradually removing kinetic energy from the nuclei by multiplying velocities with a factor (set to `0.95` in our input, so 5% of the kinetic energy will be removed at each step). 

```
mkdir ../6_annealing
cd ../6_annealing
cp ../5_mimic_prep/mimic.tpr .
cp ../5_mimic_prep/annealing.inp .
```
:warning: **Update the path in the `PATH` keyword in the `&MIMIC` section - no white spaces!**  

:warning: **Keep the `LMAX` values consistent with what we used for the dipole moment calculation in vacuum, namely `S` for `H` and `P` for `C` and `O`. By default, all these have been set to `S` by the preprocessor.**

<details>
<summary>:information_source: CPMD input for annealing command</summary> 
<hr>   
  
The `&CPMD` section contains few keyword which have not been explained in detail yet:  
  * `MOLECULAR DYNAMICS BO`, to perform a molecular dynamics run. `BO` stands for a Born-Oppenheimer type of MD. An alternative option is `CP`, which stands for a Car-Parrinello type MD.  
  * `ANNEALING IONS`, specifies to perform a simulated annealing simulation, where kinetic energy is gradually removed from the nuclei by multiplying velocities with a factor specified in the following line (`0.99` here, so 1% of the kinetic energy removed at each step). 
  * `NEW CONSTRAINTS`, which swtiches on the new constraint solver specifically designed for the MiMiC interface
  * `TEMPERATURE`, the initial temperature for the atoms (in Kelvin), specified in the following line (`300` in this case). Here we choose the  temperature at which we equilibrated the system at force field level.  
  * `TIMESTEP`, time step (in a.u.), specified in the following line  (`10` in this case). The choice of the timestep is crucial to have a stable simulation, and at the same time to optimize the time for the computation. For BO MD, a time step of 10 a.u. (~ 0.24 fs) is usual.  
  * `MAXSTEP`, maximum number of steps for MD to be performed, specified in the following line.  
  * `TRAJECTORY SAMPLE`, the frequency at which to save atomic positions, velocities and optionally forces. It is specified with a number, corresponding to the number of steps, specified in the following line. If this is set to `0`, no `TRAJECTORY` file will be written.   
  * `STORE`, the fequency at which to update the `RESTART` file. It is specified with a number, corresponding to the number of steps, specified in the following line.   
  * `RESTFILE`, the number of distinct `RESTART` files generated during CPMD runs, specified in the next line.
<hr>   
</details>

A QM/MM simulation with the MiMiC interface will require running at the same time two different (parallel) processes, one for CPMD and one for GROMACS. In the largest majority of the cases one core for GROMACS is enough, and this is what we will choose use. Run the annealing (all command as one line), making sure to specify the correct path for the mimic folder, instead of `path-to-mimic-folder`, to provide CPMD the pseudopotentials:
```
mpirun -n 7 cpmd.x annealing.inp path-to-mimic-folder/mimic/examples/acetone/data/pseudopotentials > annealing.out : -n 1 gmx_mpi_d mdrun -deffnm mimic -ntomp 1 &
```
:warning: **When running on a cluster, you need to insert this comamnd line in a bash script to the workload manager (e.g. Slurm).** 
A submission script example for G100  is provided in `/g100_work/tra22_qmmm/Resources/`, copy it in the current folder and edit it accordingly
```
cp /g100_work/tra22_qmmm/Resources/example_sbatch.sbatch .
```
<!--- Specific example for CECAM school on G100 - generalize--->


While the simulation runs you can monitor the decreasing temperature (third column of the `ENERGIES` file):
```
tail -f ENERGIES
```
When the temperature reaches about 10 K you can "softly" stop the calculation (in order to allow CPMD to write a RESTART file) by typing on the prompt line:
```
touch EXIT
```
Note that in the general case, a better practice is to anneal to a lower temperature, around 1K. For a simple system such as acetone this should not be a problem and allows us to save some time during the tutorial.

The last configuration will be stored in the `RESTART.1` file (the last restart file written is anyway specified in the `LATEST` file). Wait until CPMD finishes writing the file and then verify that CPMD and GROMACS processes are stopped.  In case the "soft" does not work, manually kill the processes using
```
kill %1
```
where `%1` refers to the first job in background submitted by the user, CPMD in our case. It might be needed also to stop manually the GROMACS process after CPMD.


The `annealing.out` file reports some new sections with respect to the CPMD output file obtained for the dipole moment calculation in vacuum (which was a pure QM calculation):  
 * The energy report shows the different energetic contributions
    ```
    (K+E1+L+N+X+MM+QM/MM)  TOTAL ENERGY =          -44.53592240 A.U.
     (K+E1+L+N+X)        TOTAL QM ENERGY =          -36.46982014 A.U.
     (MM)                TOTAL MM ENERGY =           -8.02852010 A.U.
     (QM/MM)          TOTAL QM/MM ENERGY =           -0.03758217 A.U.
     (K)                  KINETIC ENERGY =           28.12240741 A.U.
     (E1=A-S+R)     ELECTROSTATIC ENERGY =          -27.50067179 A.U.
     (S)                           ESELF =           29.92067103 A.U.
     (R)                             ESR =            1.78558591 A.U.
     (L)    LOCAL PSEUDOPOTENTIAL ENERGY =          -29.86421972 A.U.
     (N)      N-L PSEUDOPOTENTIAL ENERGY =            3.61298065 A.U.
     (X)     EXCHANGE-CORRELATION ENERGY =          -10.84031668 A.U.
              GRADIENT CORRECTION ENERGY =           -0.57543727 A.U.
    ```
 * After the force initialization section, BO MD begins: for each MD step, the wavefunction is re-converged. The MD step number is indicated as `NFI` (number of finite iterations), and for MD each step different quantities are reported: temperature, calculated as the kinetic energy divided by the degrees of freedom (`TEMPP`), quantum DFT Kohn-Sham electron energy, equivalent to the potential energy in classical MD (`EKS`), total energy in a classical MD (`ECLASSIC`), mean squared displacement of the atoms from the initial coordinates (`DIS`), and the time took to calculate this step (`TCPU`). Since in BO MD the wavefunction is re-converged, for each step information about the convergence are reported.
    ```
      NFI   TEMPP           EKS      ECLASSIC         DIS    TCPU
        1   297.1     -44.53592     -43.09507   0.844E-04   16.34
        INFR         GEMAX            ETOT         DETOT     TCPU
            1     8.907E-04      -36.507197     0.000E+00     3.02
            2     4.218E-04      -36.507428    -2.304E-04     1.32
            3     2.169E-04      -36.507444    -1.607E-05     1.34
            4     1.922E-04      -36.507447    -3.256E-06     1.37
            5     9.735E-05      -36.507447    -5.238E-07     1.36
            6     3.486E-05      -36.507448    -1.715E-07     1.40
            7     2.119E-05      -36.507448    -9.343E-08     1.41
            8     1.248E-05      -36.507448    -5.398E-08     1.39
            9     5.057E-06      -36.507448    -2.455E-08     1.46
        2   294.2     -44.53611     -43.10943   0.172E-03   82.96
        INFR         GEMAX            ETOT         DETOT     TCPU
            1     8.942E-04      -36.507225     0.000E+00     3.02
            2     4.189E-04      -36.507455    -2.304E-04     1.34
            3     2.159E-04      -36.507472    -1.613E-05     1.35
            4     1.981E-04      -36.507475    -3.273E-06     1.33
            5     9.894E-05      -36.507475    -5.344E-07     1.40
            6     3.474E-05      -36.507476    -1.739E-07     1.42
            7     2.159E-05      -36.507476    -9.483E-08     1.10
            8     1.275E-05      -36.507476    -5.612E-08     1.12
            9     5.119E-06      -36.507476    -2.584E-08     1.09
        ...
    ```   
  * When the simulation is completed (due to the maximum number of steps/computational time reached or "soft" `EXIT` message received), a summary of averages and root mean squared deviations for some of the monitored quantities is reported. This is useful to detect unwanted energy drifts or too large fluctuations in the simulation:   
     ```
      RESTART INFORMATION WRITTEN ON FILE                  ./RESTART.1
     SUBSYSTEM            1 SR ATOMS PER PROCESS GROUP         493
     SUBSYSTEM            1 SR ATOMS PER PROCESS         247
     SUBSYSTEM            1 LR ATOMS PER PROCESS GROUP        1035
     SUBSYSTEM            1 LR ATOMS PER PROCESS         518

     ****************************************************************
     *                      AVERAGED QUANTITIES                     *
     ****************************************************************
                               MEAN VALUE <x>   DEVIATION <x^2>-<x>^2
     IONIC TEMPERATURE                 26.38              46.0235    
     DENSITY FUNCTIONAL ENERGY    -45.672099             0.417524    
     CLASSICAL ENERGY             -45.544157             0.614735    
     NOSE ENERGY ELECTRONS          0.000000              0.00000    
     NOSE ENERGY IONS               0.000000              0.00000    
     ENERGY OF CONSTRAINTS          0.000000              0.00000    
     ENERGY OF RESTRAINTS           0.000000              0.00000    
     BOGOLIUBOV CORRECTION          0.000000              0.00000    
     ION DISPLACEMENT            1.27023                 0.844647    
     CPU TIME                        35.9728
     ```
 * To have a visual check that the annealing proceeded as expected, you can have a look at the temperature and the physical energy to check they correctly stabilized. You can use any program to visualize this, in particular using `gnuplot` the commands needed to plot `TEMPP` (column `3`) and `ECLASSIC` (column `5`), are:
    ```
    gnuplot
    ```
    ```
    set multiplot layout 1, 2
    set title "Temperature"
    set xlabel "MD step"
    set ylabel "Temperature (K)"
    plot 'ENERGIES' u 1:3 w l lt 1 title "TEMPP"
    set title "Physical Energy"
    set xlabel "MD step"
    set ylabel "Energy (a.u.)"
    plot 'ENERGIES' u 1:5 w l lt 2 title "ECLASSIC"
    ```
    ```
    quit
    ```
Here you can see an example of the evolution of these two quantities during the test:

  <p align = "center">
  <img src="./data/fig/annealing.png" alt="annealing" width="600"/>
  </p>
  <p align = "center">
  <figcaption align = "center"><a>Evolution of temperature and physical energy along the MD simulation (BO - annealing).</a></figcaption>
  </p>
Note: sometimes the "soft" `EXIT` command can cause problems for some of  the  quantities printed in the output, in particular the MM energy may be 0. You can easily check that by looking at the corresponding line in the output and it will be evident from the `ENERGIES` file. This should not cause problems in the following since we will only use the final configuration, but may result in a plot slightly different from the example reported (the last point will be clearly higher).

### Test - NVE MD (optional!)
**If you are short on time during the tutorial you may skip this section.**

In general, it is a good idea to verify that the final configuration obtained after the annealing is a physically "reasonable" minimum energy configuration and that the BO MD has not brought the system in a very improbable configuration. A good test is to run a simulation in an NVE ensemble monitoring temperature (`TEMPP`) and physical energy (`ECLASSIC`): if after some steps these two quantities stabilize, then it is possible to be confident that the `RESTART.1` file previously obtained contains a good minimum energy structure. On the other hand, if energy and/or temperature continuously increase, that means that a good structure has not yet been obtained, and another annealing procedure is required, usually starting from a different configuration (for example, after heating the system at 300 K in order to move the system away from that “wrong” energy potential basin), or changing the annealing parameters (for example the annealing factor in `ANNEALING IONS`).

The test can be performed by the following procedure:
* Create a new folder and copy in it all the files needed (we will modify the input file used for the annealing):
    ```
    mkdir ../7_test
    cd ../7_test
    cp ../6_annealing/mimic.tpr .
    cp ../6_annealing/RESTART.1 ./RESTART
    cp ../6_annealing/annealing.inp ./test.inp
    ```
* Modify `test.inp` file so that the `&CPMD` section appears as:
    ```
    &CPMD
    RESTART COORDINATES VELOCITIES WAVEFUNCTION
    MIMIC
    MOLECULAR DYNAMICS BO
    NEW CONSTRAINTS
    ISOLATED MOLECULE
    TIMESTEP
     10.0
    MAXSTEP
     1000
    TRAJECTORY SAMPLE
     0
    &END
    ```
:warning: **Update the path in the `PATH` keyword in the `&MIMIC` section - no white spaces!**  
* Run the test
    ```
    mpirun -n 7 cpmd.x test.inp path-to-mimic-folder/mimic/examples/acetone/data/pseudopotentials > test.out : -n 1 gmx_mpi_d mdrun -deffnm mimic -ntomp 1 &
    ```
* Monitor the simulation:
    ```
    tail –f ENERGIES
    ```
* When the simulation is completed, you can have a look at the temperature and the physical energy to check they correctly stabilized. You can use any program to visualize this, in particular using `gnuplot` the commands needed to plot `TEMPP` (column `3`) and `ECLASSIC` (column `5`), are:
    ```
    gnuplot
    ```
    ```
    set multiplot layout 1, 2
    set title "Temperature"
    set xlabel "MD step"
    set ylabel "Temperature (K)"
    plot 'ENERGIES_1000' u 1:3 w l lt 1 title "TEMPP"
    set title "Physical Energy"
    set xlabel "MD step"
    set ylabel "Energy (a.u.)"
    plot 'ENERGIES_1000' u 1:5 w l lt 2 title "ECLASSIC"
    ```
    ```
    quit
    ```
Here you can see an example of the evolution of these two quantities during the test:

  <p align = "center">
  <img src="./data/fig/test_NVE.png" alt="testNVE" width="600"/>
  </p>
  <p align = "center">
  <figcaption align = "center"><a>Evolution of temperature and physical energy along the MD simulation (BO - NVE).</a></figcaption>
  </p>

### Heating
If the test is successful (or if you skipped it), we can come back to the configuration obtained by the annealing procedure and start heating the system up to the room temperature. One way to do this is to increase the target temperature of a thermostat (coupled to the system) linearly at each step by performing a usual BO MD. A simple Berendsen-type thermostat can be used in the heating phase: it does not fully preserve the correct canonical ensemble but we are not interested to this feature at this stage, while it is numerically fast and more stable than alternative algorithms.
  
You can heat the system by performing the following procedure:
* Create a new folder and copy in it all the files needed (we will modify the input file used for the annealing):  
    ```
    mkdir ../8_heating
    cd ../8_heating
    cp ../6_annealing/mimic.tpr .
    cp ../6_annealing/RESTART.1 ./RESTART
    cp ../6_annealing/annealing.inp ./heating.inp
    ```
* Modify `heating.inp` file so that the `&CPMD` section appears as:
    ```
    &CPMD
    RESTART COORDINATES VELOCITIES WAVEFUNCTION
    MIMIC
    MOLECULAR DYNAMICS BO
    NEW CONSTRAINTS
    ISOLATED MOLECULE
    TEMPERATURE RAMP
    T_init  340.0  20
    BERENDSEN IONS
    T_init  5000
    TIMESTEP
     10.0
    MAXSTEP
     1500
    TRAJECTORY SAMPLE
     1
    &END
    ```
  :warning: **Update the path in the `PATH` keyword in the `&MIMIC` section - no white spaces!**  

  With respect to the input file used to perform the test, two additional keywords are required in the `&CPMD` section:
  1. `TEMPERATURE` with the option `RAMP`, where 3 numbers have to be specified on the line below the keyword: initial temperature (`T_init`, in K), target temperature (in K) and the ramping speed (in K per atomic time unit, to get the change per time step you have to multiply it with the value of `TIMESTEP`). In particular, we set a target temperature slightly higher than our real target of 300 K, since the thermostat may require a long time before actually reaching the target temperature specified.
  2. `BERENDSEN` with the option `IONS`, where 2 numbers  have to be specified on the line below the keyword: the target temperature for the termostat, which in our case is the  initial one (`T_init`) and the time constant τ of the thermostat (in a.u.). The suggested value of 5000 a.u., corresponding to ~0.12 ps, is a reasonable value for the system.
  
  The value of `T_init` corresponds to the final temperature of the annealing procedure, from which we are starting the heating. You can read it from the `ENERGIES` file generated in the annealing (third column) and set it accordingly in the `heating.inp` file
    ```
    tail -1 ../6_annealing/ENERGIES
    ```

* Run the heating
    ```
    mpirun -n 7 cpmd.x heating.inp path-to-mimic-folder/mimic/examples/acetone/data/pseudopotentials > heating.out : -n 1 gmx_mpi_d mdrun -deffnm mimic -ntomp 1 &
    ```
* Monitor the temperature during the simulation (third column):
    ```
    tail –f ENERGIES
    ```
* If a temperature of approximately 300 K is reached before the `MAXSTEP`, you can stop the simulation:
    ```
    touch EXIT
    ``` 
* If you need to continue heating after reaching MAXSTEPS, add two keywords in the `&CPMD` section in the `RESTART` line of `heating.inp`:
  1. `LATEST`, which ensures to use the lastest written `RESTART` file (whose name is contained in the `LATEST` file)
  2.  `ACCUMULATORS`, which ensures that new energy data and trajectory will be appended to your existing files. Run the job in the same working directory as before. 

    ```
    RESTART COORDINATES VELOCITIES WAVEFUNCTION LATEST ACCUMULATORS
    ``` 
* When the simulation is completed, you can have a look at how the different quantities evolved during the heating phase. In particular, you can check how the temperature rises. You can use any program to visualize this, in particular using `gnuplot` the commands needed to plot `TEMPP` (column `3`) are:
    ```
    gnuplot
    ```
    ```
    set title "Temperature"
    set xlabel "MD step"
    set ylabel "Temperature (K)"
    plot 'ENERGIES' u 1:3 w l lt 1 title "TEMPP"
    ```
    ```
    quit
    ```
Here you can see an example of the rise in temperature during the heating. In particular, to obtain this plot we run a longer heating than the `MAXSTEP` we set previously, and we had to to restart it from time to time to be able to reach the desired temperature of 300K (as explained below):
  <p align = "center">
  <img src="./data/fig/heating.png" alt="heating" width="600"/>
  </p>
  <p align = "center">
  <figcaption align = "center"><a>Evolution of the temperature along the MD simulation (BO - NVT heating).</a></figcaption>
  </p>


Note that the heating phase can be challenging: it is not always easy to get the  temperature to stabilize to the desired value. The temperature specified in the `RAMP` will be eventually reached, but it may require a large amount of time, i.e. many more steps than the ones performed. What you can do in practice, if your system seems to be stabilized at a lower temperature than desired, is to restart the heating procedure from that intermediate point. You will then set the temperature for that configuration as `T_init`, and you can try to set a higher target temperature or a faster ramping speed. 

You can do it by using the additional option `GEOFILE` in the `RESTART` keyword (after having extracted the desired `GEOMERY`), and this  procedure to restart is explained in the next section. Anyway, even if to run your own system you will of course need to compelte the heating phase, for this tutorial we sugget to move to the next section even if you did not manage to obtain a well equilibrated configuration at QM/MM level: we will provide one for you, so you can see also how to start a production run. 

### Production Run
Now that the system is well equilibrated, we are finally ready to run a QM/MM MD at room conditions.  
  
You can run the production by performing the following procedure:
* Create a new folder and copy in it all the files needed (we will modify the input file used for the heating):  
    ```
    mkdir ../9_production
    cd ../9_production
    cp ../8_heating/mimic.tpr .
    cp ../8_heating/heating.inp ./prod.inp
    cp ../8_heating/RESTART.1 ./RESTART
    ```
* Modify `prod.inp` file so that the `&CPMD` section appears as:
    ```
    &CPMD
    RESTART COORDINATES VELOCITIES WAVEFUNCTION GEOFILE
    MIMIC
    MOLECULAR DYNAMICS BO
    DIPOLE DYNAMICS
    NEW CONSTRAINTS
    ISOLATED MOLECULE
    DIPOLE DYNAMICS
    NOSE IONS
    300  4000
    TIMESTEP
     10.0
    MAXSTEP
     1000
    TRAJECTORY SAMPLE
     100
    STORE
    100
    RESTFILE
    10
    &END
    ```
  :warning: **Update the path in the `PATH` keyword in the `&MIMIC` section - no white spaces!**  

  With respect to the input file used to perform the heating, we made some modifications:
  1. The `GEOFILE` option has been added to the `RESTART` keywod. To perform the restart, this option will read old ionic positions and velocities from the file `GEOMETRY`. Note that a `RESTART` file need to be present as well, to read informations about the system not present in the geometry file, such as the atom elements.
  2. The keywords `DIPOLE DYNAMICS` have been added which captures dipole information during the production run every `NSTEP` iteration in MD and saves it in an output file named `DIPOLE`. The `NSTEP` value is read from the next line if the keyword `SAMPLE` is present. But without this keyword and value, the default is `1` (every time step). Here we simply capture the dipole information at everystep. We will use the data stored in the `DIPOLE` file in **Dipole Calculation - method 1** section.  
  3. `BERENDSEN` thermostat for ions has been replaced with `NOSE`, corresponding with Nose-Hoover chains. As mentioned before, Berendsen a fast and stable thermostat, but does not properly samples the canonical ensemble. On the other hand, Nose-Hoover preserves the Maxwell distribution of the velocities and allows sampling the correct canonical ensemble, providing a NVT ensemble for a system in equilibrium. After the `NOSE IONS` keyword, 2 numbers are specified: the target temperature for the termostat, which in our case is 300 K, and the thermostat frequency in cm<sup>-1</sup>, here 4000 cm<sup>-1</sup>. Concerning the choice of the thermostat frequency, at which the energy transfer from/to the thermostat happens, it is important not to select a resonance vibrational frequency of your system.
  4. The `MAXSTEP` has been increased to 10000 steps. Considering the timestep of choise (10 a.u.), this corresponds to a total simulation time of 100000 a.u., i.e. ~2.4 ps. 
  5. The trajectory is stored every 100 steps (`TRAJECTORY SAMPLE` option), and during the simulation 10 restart files will be saved. This is selected choosing to update the `RESTART` file every 1000 steps (`STORE` options), and saving 10 distinct `RESTART` files (`RESTFILE` option). In this way, a sequence of files `RESTART.1`, `RESTART.2`, ..., `RESTART.10` will be produced during the dynamics. We will calculate the dipole moment of the acetone molecule for each of these configurations.
* We want to start the production from a reasonably heated configuration. This can be done extracting the coordinates and velocities of a step of choice, where the temperature has reached the desired value. We will save this information in a `GEOMETRY` file, which will be used as starting point thanks to the `GEOFILE` option we added to the `RESTART` keywod.

:warning:  Even in case you did not manage to obtain a well-equilibrated structure and you will use the `GEOMETRY` file that we provide as starting point for the production (instructions below) it is useful to read the process described here to extract a `GEOMETRY`  file from a `TRAJECTROY` since you will be likely perform this step when working with MiMiC with your systems.
  
  We provide a useful script to do that, called `geofile_extract.py`, even if this can in principle be easily done manually from the `TRAJECTORY` file. In order to extract the coordinates and velocities, we need to choose a step at which doing it. We can do it using the `ENERGIES` file produced during the heating process. It is important to keep in mind that how often coordinates and velocities have been stored. In our case, we set `TRAJECTORY SAMPLE 1`, i.e. we stored coordinates and velocities each steps, so we are sure every step we see in the `ENERGIES` file will have a corresponding set  of positions and velocities in the `TRAJECTORY` file (but this may not be the case). Inspect the `ENERGIES` file and select a step number (first column) where the temperature (third column) is aroud 300K
    ```
    cd ../8_heating
    vi ENERGIES
    ```
  You will see something similar to
  <p align = "center">
  <img src="./data/fig/heating_energies.png" alt="heating_eng" width="600"/>
  </p>
  <p align = "center">
  <figcaption align = "center"><a>Example output of BO MD Heating simulation for Acetone.</a></figcaption>
  </p>
 In this example step 3477 seems a good choice as it is near 300K and shows stability in that range for awhile. You could manually extract the coordinates and the `TRAJECTORY` file, and convert it in the `GEOMETRY` format. Otherwise, you can use the `geofile_extract.py` script to extract the corresponding geometry.

  ```
  wget https://gitlab.com/MiMiC-projects/mimic_helper/-/raw/main/scripts/geofile_extract.py
  python geofile_extract.py ../8_heating/TRAJECTORY ../8_heating/ENERGIES XXX
  ```

  This will extract step `XXX` (which we would fill in as `3477` in our example) from the `TRAJECTORY` file, and check the corresponding temperature in the `ENERGIES` file. You will see in the ouput information about the temperature of the step selected, and the location of the corresponding geometry file. In case `geofile_extract.py` script does not work in your working directory, copy it to the `8_heating` directory and execute it locally in that directory and go back to production directory: 
    ```
    python3 geofile_extract.py TRAJECTORY ENERGIES XXX
    ```
  This will generate a geometry file `GEO_TTTK`, where `TTT` is the temperature of the configuration. Go back to the production run folder and copy there the extracted geometry file, renaming it `GEOMETRY` to use it as starting point for the production run
    ```
    cd ../9_production
    cp ../8_heating/GEO_TTTK ./GEOMETRY
    ```  
* In case you did not manage to obtain a well-equilibrated configuration at QM/MM level, you can start the production run using the `GEOMETRY` provided. Copy that in the current folder with the following command
    ```
    cp ../../data/GEO_295K ./GEOMETRY
    ```
* Now we have all the elements to run the production
    ```
    mpirun -n 7 cpmd.x prod.inp path-to-mimic-folder/mimic/examples/acetone/data/pseudopotentials > prod.out : -n 1 gmx_mpi_d mdrun -deffnm mimic -ntomp 1 &
    ```
* You can monitor different quantities of interest during the production run, for example the temperature (`TEMPP`, third column) and the physical energy (`ECLASSIC`, fifth column) of the system:
    ```
    tail –f ENERGIES
    ```
* When the simulation is completed, you can have a look at how these two quantities evolved during the production phase. You can use any program to visualize this, in particular using `gnuplot` the commands needed to plot `TEMPP` (column `3`) are:
`TEMPP` (column `3`) and `ECLASSIC` (column `5`), are:
    ```
    gnuplot
    ```
    ```
    set multiplot layout 1, 2
    set title "Temperature"
    set xlabel "MD step"
    set ylabel "Temperature (K)"
    plot 'ENERGIES' u 1:3 w l lt 1 title "TEMPP"
    set title "Physical Energy"
    set xlabel "MD step"
    set ylabel "Energy (a.u.)"
    plot 'ENERGIES' u 1:5 w l lt 2 title "ECLASSIC"
    ```
    ```
    quit
    ```
Here you can see an example of the evolution of these two quantities during the test:
<p align = "center">
<img src="./data/prod_run/prod_nocons.png" alt="production" width="600"/>
</p>
<p align = "center">
<figcaption align = "center"><a>Evolution of of temperature and physical energy along the MD simulation (BO - NVT) during first 5000 steps.</a></figcaption>
</p>

  
* A very good practice when you perform a simulation is to look at your trajectory through some visualization tool. This in particular can be extremely helpful when you notice some strange behavior of some physical quantity: the most of the problems are immediately identified by visual inspection.
To visualize a the trajectory you can for example use VMD. There are several options to do so:
   
    1. By setting the option `TRAJECTORY XYZ` in the `&CPMD` section the output file `TRAJEC.xyz` is produced. This can be directly loaded into VMD 
         ```
          vmd TRAJEC.xyz
         ```

    2. In principle, all the information needed by VMD are present in two distinct files: the `GEOMETRY.xyz` file contains the information about the atom types, and the `TRAJECTORY` file contains the cordinates at different timesteps. You can load the `GEOMETRY.xyz` file in VMD and input in it the `TRAJECTORY`.
         ```
          vmd GEOMETRY.xyz
         ```
       Then from the VMD menu right click on the `GEOMETRY.xyz` molecule, select `Load Data Into Molecule` and load the `TRAJECTORY` file, selecting `CPMD` in the `Determine file type` dropdown menu. Be aware that in this way the first frame will be the configuration in the `GEOMETRY.xyz`, while the actual trajectory will be visualized from.
  
    3. Convert the `TRAJECTORY` file into the `xyz` format, making it easier the loading into VMD even in the case of not having specified the option `TRAJECTORY XYZ` in the `&CPMD` section. For example, you can use the python script `traj_xyz.py` 
  
       ```
        wget https://gitlab.com/MiMiC-projects/mimic_helper/-/raw/main/scripts/traj_xyz_convert.py
        python3 traj_xyz_convert.py TRAJECTORY GEOMETRY.xyz
       ```
    this will generate a `TRAJECTORY.xyz` file in the current folder. This can easily loaded into VMD as in option 1.

  
  
<!--- visualization example and comment --->
## Dipole Calculation - method 1
One way to monitor the dipole moment over the production is by analyzing the data in the `DIPOLE` output file (produced thanks to the keyword `DIPOLE DYNAMICS` added to the input file).  The first column of this file contains the step, columns 2 to 4 are the electronic, ionic and total contribution to the dipole moment. Due to later changes in CPMD code, colums 5 to 7 contain the same information of columns 2 to 4. *All the dipole moments are divided by the volume of the quantum box.*

We can analyze the dipole moment along our QM/MM MD simulations with a simple procedure (note that all this operation can be easily done manyally, but for simplicity we provide command lines to perform these mathematical calculations):
* Compute the QM box volume from the values in the `CELL` keyword of the `&CPMD` section
    ```
    grep 'CELL' prod.inp -A 1 |  tail -1 | awk '{print $1*$1*$2*$1*$3}' 
    ```
    where the A B/A C/A sides of the box are extracted from the line following the keyword `CELL` (by finding it using `grep` and obtaining the following line with `tail`), and are summed together using the command `awk`.
* Average the values of the second column, multiplied by the cell volume. The following command line will print out the average and the standard deviation of the second column of the file  `DIPOLE`. Run the following line, substituting the volume of the cell box found before to `XXX`
    ```
    awk 'CELL_VOLUME=XXX  {if($2!=""){count++;sum+=($2*CELL_VOLUME)};y+=($2*CELL_VOLUME)^2} END{sq=sqrt((y)/NR-(sum/NR)^2);sq=sq?sq:0;print "Mean = "sum/count ORS "S.D = ",sq}'  DIPOLE 
    ```

Now, you can compare the dipole moments obtained in gas phase ([see Introduction.md](https://gitlab.com/MiMiC-projects/mimic-school/-/blob/Ex1-review/Day1_Acetone_Ex/1_introduction.md)) and in solution (from BO QM/MM-MD). In gas phase we see dipole moments around 1.2 a.u. or 3.1 Debye. 

## Dipole Calculation - method 2

  As a last step we want to evaluate the dipole moment from the information previously collected during the produciton run, and subsequently to calculate their average value. This average value represents our estimate for the dipole moment of acetone in water, where both the temperature and entropic effects due to the solvent environment are taken into account.

  This can be done by performing the following procedure:
* Create a new folder and copy in it all the files needed (we will modify the input file used for the annealing):  
  ```
  mkdir ../10_dipole
  cd ../10_dipole
  cp ../9_production/mimic.tpr .
  cp ../9_production/prod.inp ./dipole.inp
  ```
* Modify `dipole.inp` file so that the `&CPMD` section appears as:
    ```
    &CPMD
    MIMIC
    RESTART WAVEFUNCTION COORDINATES LATEST
    PROPERTIES
    RESTFILE
    0
    &END
    ```
   where the keyword `RESTFILE` with value `0` guarantees that CPMD does not write any `RESTART` file at the end of the dipole calculation, which is not needed now, and avoids to overwrite any `RESTART` file copied from the `9_production` folder. To perform a dipole calculation you also need to add the following section (as we did before to calculate the dipole moment in vacuum):
    ```
    &PROP
    DIPOLE MOMENT
    &END
    ```
  :warning: **Update the path in the `PATH` keyword in the `&MIMIC` section - no white spaces!**  
* The 10 calculations will be performed using each `RESTART.X` file, and this can be easily done editing the `LATEST` file (this is why the `LATEST` option has heen added). For this reason, before running each dipole calculation, you need to replace the name of the `RESTART.X` in the first line of the `LATEST` file. This can be easily done with the suggested command:

  ```
  for i in {1..10}; do
     ln -fs ../9_production/RESTART.$i RESTART
     mpirun -n 7 cpmd.x dipole.inp path-to-mimic-folder/mimic/examples/acetone/data/pseudopotentials > dipole_$i.out : -n 1 gmx_mpi_d mdrun -deffnm mimic -ntomp 1
  done
  ```
 * When the last process is completed, you can extract each value of the dipole moment from the output files by looking at each output file :
    ```
    grep -A 5 DIPOLE  dipole_X.out  | tail -6 | head -4
    ```
Now, you can compare the dipole moments obtained in gas phase ([see Introduction.md](https://gitlab.com/MiMiC-projects/mimic-school/-/blob/Ex1-review/Day1_Acetone_Ex/1_introduction.md)) and in solution (from BO QM/MM-MD). In gas phase we see dipole moments around 1.2 a.u. or 3.1 Debye. 
<!--- Add comments and comparison with dipole in vacuum--->

Did you try both methods for calculating the dipole moment? Which one agrees better with published values for solvated acetone at room temperature? Some published values are between 1.5-2.0 a.u. (see [this study]((https://doi.org/10.1016/j.cplett.2011.04.015))? 
