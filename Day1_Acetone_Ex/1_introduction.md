
# Acetone Tutorial
Acetone is the organic compound with the formula (CH<sub>3</sub>)<sub>2</sub>CO, representing the simplest example of ketone. It is a colorless, volatile, flammable organic solvent naturally occurring in plants, trees, forest fires, vehicle exhaust, and as a breakdown product of animal fat metabolism. It dissolves in water and is used  to make plastic, fibers, drugs, and other chemicals, and also to dissolve other substances.

<p align = "center">
<img src="./data/fig/Acetone-3D-balls.png" alt="acetone3D" width="200"/>
</p>
<p align = "center">
<figcaption align = "center"><a>Ball-and-stick model of acetone. <a href="https://en.wikipedia.org/wiki/Acetone#/media/File:Acetone-3D-balls.png" title="Wikipedia">[Image source]</a></figcaption>
</p>

---

For this tutorial we will assume that you cloned the github repository, hence you will have the provided folders and files in the same locations.

```
git clone https://gitlab.com/MiMiC-projects/mimic-school.git
```
This command will clone the `mimic-school` repository in the current folder. Navigate the repository and create your own directory to run this exercise 

```
cd mimic-school/Day1_Acetone_Ex
mkdir  solution
cd  solution
```


## Dipole Moment in Vacuum with CPMD
We will start by using CPMD to calculate the dipole moment of acetone in vacuum: ensure that you either have CPMD loaded into your coding environment or called upon in your run script. Example run scripts for SLURM schedulerss are included in the `data` folder. For example, if you want to load CPMD into your coding environment:

```
module load CPMD
```

Create a folder where we are going to perform the calculations with CPMD:

```
mkdir  1_dipole_vac
cd 1_dipole_vac
```

### Geometry optimization

In order to calculate the dipole moment of the acetone in vacuum we first have to optimize the geometry of the acetone molecule. The input file is provided in the `data` folder for this tutorial. Copy this file in the current folder:


```
cp ../../data/opt_geo.inp .
```

The geometry together with instructions for CPMD to run the optimization can be found in this file.
<details>
<summary>:information_source: CPMD input files</summary>
<hr> 

CPMD input files are organized in sections that start with `&NAME` and end with `&END`. Inside sections, specific keywords are present. The sequence of the sections does not matter, nor does the order of keywords (except in some special case reported in the manual).  All keywords have to be in upper case, otherwise they will be ignored. 

In particular, the input file `opt_geo.in`, used to perform a geometry optimization, contains the following sections:
* `&INFO`: optional section, allowing the user to insert comments about the calculation, which will be repeated in the output file.
* `&CPMD`: required section, specifying the instructions for the calculation. In this case, a geometry optimization (`OPTIMIZE GEOMETRY`) will be performed. The `XYZ` suboption specifies that the final structure will be also saved in xyz format in a file called `GEOMETRY.xyz`, and the ’trajectory’ of the optimization in a file named `GEO_OPT.xyz`. Convergence criteria for wavefunction and geometry convergence are also specified in this section.
* `&SYSTEM`: required section, containing simulation cell and plane wave parameters. The keywords `SYMMETRY`, `CELL` and `CUTOFF` are required and define the (periodic) symmetry, shape and size of the simulation cell, as well as the plane wave energy cutoff (i.e. the size of the basis set), respectively. The keyword `ANGSTROM` specifies that the atomic coordinates, the supercell parameters and several other parameters are read in Angstrom (pay attention: default units for CPMD are atomic units (a.u.), which are always used internally). 
* `&DFT`: optional section, reporting the exchange and correlation functional and related parameters. In this case, the `BLYP` functional is used (local density approximation is the default). 
* `&ATOMS`: required section, containing atoms and pseudopotentials and related parameters. The input for a new atom type is started with a `*` in the first column. This line further contains the file name where to find the pseudopotential information, and several other possible labels such as `KLEINMAN-BYLANDER` in our case, which specifies the method to be used for the calculation of the nonlocal parts of the pseudopotential (this approximation make the nonlocal parts calculation extremely fast but in general it also keeps also high accuracy). The next line contains information on the nonlocality of the pseudopotential: the maximum l-quantum number can be specified with `LMAX= l ` where `l` is `S` for l=0, `P` for l=1 or `D` for l=2. The following line contains the number of atoms of the current type, followed by the coordinates for this atomic species.


For more details see [CPMD manual](https://www.cpmd.org/wordpress/CPMD/getFile.php?file=manual.pdf) and [CPMD documentation](https://www.cpmd.org/wordpress/index.php/documentation/#).
<hr> 
</details>

We will perform the optimization at DFT level, using the BLYP functional. This is indicated in the `&DFT` section of the input file:

```
&DFT
 FUNCTIONAL BLYP
&END 
```
To run CPMD (and any DFT calculation in general), you need to provide pseudopotentials as second input in the `cpmd.x` command (see below). The pseudopotentials needed for this tutorial are provided in this repository: `mimic-school/Day1_Acetone_Ex/data/pseudopotentials`.

<details>
<summary>:information_source: More about Pseudopotentials</summary>
 <hr> 
 
The selection of proper pseudopotentials and the corresponding plane wave cutoff are crucial to obtain good results with a CPMD calculation. The pseudopotentials to use for describing different atomic species are specified in the CPMD input files in the `&ATOMS` section. The corresponding pseudopotential files need to be provided as input when running CPMD.

Pseudopotential files usually have `.psp` extension and contain different sections. In particular, the `&INFO` section contains a `Pseudopotential Report` with relevant information, and information for the pseudopotential and the pseudo wavefunction are contained in the `&POTENTIAL` and `&WAVEFUNCTION`, respectively. The columns in the `&POTENTIAL` section provide useful information for the choice of `LMAX`: the highest possible value of depends on the highest angular momentum for which a 'channel' was created in the pseudopotential. In the `&POTENTIAL` section, the first column is the radius, the next ones are the orbital angular momenta (s, p, d, f,. . . ). It is possible to use `LMAX` values as high as there is data foor in the pseudopotential file, but you since higher values correspond to an increase in calculation time, it is not required to use the maximum possible `LMAX` unless the physics of the problem requires this.
 
 
For more details see [CPMD manual](https://www.cpmd.org/wordpress/CPMD/getFile.php?file=manual.pdf). Pseudopotential files for different atoms can be obtained from the [CPMD documentation section](https://www.cpmd.org/wordpress/index.php/documentation/pseudo-potentials/).
<hr> 
</details>


Run the optimization using the following command (one single line), making sure to specify the correct path for the `mimic` folder, instead of `path-to-mimic-folder`. Additionally, if you're writing commands into a script you will not need to include the `&` at the end to surpress direct output. 
```
mpirun -np 2 cpmd.x opt_geo.inp path-to-mimic-folder/mimic-school/Day1_Acetone_Ex/data/pseudopotentials > opt_geo.out &
```
The calculation can be monitored using the command 

```
tail -f opt_geo.out
```
When the optimization is completed, different output files are generated. In particular, the `RESTART.1` file is a binary file containing the necessary information to restart a new calculation from the final step of the current run, i.e. the optimized geometry in this case. You can check what `RESTART` file is the last one written (the one you want to use for a new calculation) by checking the `LATEST` file.

<details>
<summary>:information_source: CPMD output files</summary>
<hr> 
CPMD output files are organized in different sections, corresponding to different phases of the calculation.

In particular, the input file `opt_geo.out`, where geometry optimization is performed, contains the following sections:
* The header, where it is possible to see when the run was started, which version of CPMD was used, and when it was compiled
 ```
 PROGRAM CPMD STARTED AT: 2022-05-09 11:23:07.287   
 SETCNST| USING: CODATA 2006 UNITS


               ******  ******    ****  ****  ******   
              *******  *******   **********  *******  
             ***       **   ***  ** **** **  **   *** 
             **        **   ***  **  **  **  **    ** 
             **        *******   **      **  **    ** 
             ***       ******    **      **  **   *** 
              *******  **        **      **  *******  
               ******  **        **      **  ******   

                          VERSION 4.3-4610

                            COPYRIGHT
                      IBM RESEARCH DIVISION
                MPI FESTKOERPERFORSCHUNG STUTTGART

                       The CPMD consortium
                  Home Page: http://www.cpmd.org
               Mailing List: cpmd-list@cpmd.org
                     E-mail: cpmd@cpmd.org


                  ***  Sep 30 2019 -- 14:15:56  ***
 ```
* Some technical information about the environment where the job was run, such as machine, user, directory, input file, and process id.
* The `&INFO` section from the input file
* A summary of some of the parameters read in from the `&CPMD` section, or the default values if not set, and the exchange correlation functionals used.
* Information about atoms (and their coordinates in a.u.), electrons and states in the system, together with pseudopotentials used and the respective settings (these are copied from the pseudopotential .psp files).
* Information about how the calculation is distributed through the cores. In this case for example, only two cores are used
 ```
 PARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARA
  NCPU     NGW     NHG  PLANES  GXRAYS  HXRAYS ORBITALS Z-PLANES
     0   17345  138720      54     978    3904       6       1
     1   17341  138669      54     977    3903       6       1
                G=0 COMPONENT ON PROCESSOR :     1
 PARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARA
 
 OPENMPOPENMPOPENMPOPENMPOPENMPOPENMPOPENMPOPENMPOPENMPOPENMPOPEN
 OMP: NUMBER OF CPUS PER TASK                                   1
 OPENMPOPENMPOPENMPOPENMPOPENMPOPENMPOPENMPOPENMPOPENMPOPENMPOPEN
 ```
* A summary of the settings read from the `&SYSTEM` section of the input file (or their default values), including some derived parameters such as density cutoff and number of plane waves.
* Generation of the initial guess for the wavefunction optimization. In this case, a superposition of atomic wavefunctions using a minimal Slater basis:
```
 GENERATE ATOMIC BASIS SET
      C        SLATER ORBITALS
        2S        ALPHA=   1.6083      OCCUPATION= 2.00
        2P        ALPHA=   1.5679      OCCUPATION= 2.00
      O        SLATER ORBITALS
        2S        ALPHA=   2.2458      OCCUPATION= 2.00
        2P        ALPHA=   2.2266      OCCUPATION= 4.00
      H        SLATER ORBITALS
        1S        ALPHA=   1.0000      OCCUPATION= 1.00
 ```
* After other initialization steps, such as the initialization of the Hessian matrix, it is possible to start with the geometry optimization. For each step, a wavefunction optimizations is performed, and then the positions of the atoms are updated according to the forces acting on them. The columns have give information about the step number (`NFI`), information about the convergence, such as the largest off-diagonal component (`GEMAX`), the average of the off-diagonal components (`CNORM`), the total energy (`ETOT`) and its change (`DETOT`), and finally the CPU time (`TCPU`):
 ```
 ================================================================
  =                  GEOMETRY OPTIMIZATION                       =
  ================================================================
  NFI      GEMAX       CNORM           ETOT        DETOT      TCPU
  EWALD| SUM IN REAL SPACE OVER                      1* 1* 1 CELLS
    1  3.104E-02   4.475E-03     -35.653984   -3.565E+01      0.71
    2  1.843E-02   1.418E-03     -36.325881   -6.719E-01      0.73
    3  1.806E-02   7.307E-04     -36.396587   -7.071E-02      0.72
  ...
   34  1.372E-07   6.696E-09     -36.430486   -1.138E-11      0.77
   35  6.576E-08   4.820E-09     -36.430486   -3.681E-12      0.77
    ATOM          COORDINATES            GRADIENTS (-FORCES)
    1  C  0.0000  0.0000  0.0000  -4.390E-02  7.606E-02 -4.687E-02
    2  C  2.5834  0.0000 -0.9134  -3.852E-02 -4.850E-02  4.907E-02
    3  C -1.2917 -2.2373 -0.9134   6.140E-02  9.279E-03  4.934E-02
    4  O  0.0000  0.0000  2.3055   3.161E-02 -5.474E-02 -6.690E-02
    5  H  2.5834  0.0000 -2.9713  -1.090E-02 -5.692E-05  9.334E-03
    6  H  3.5535  1.6803 -0.2274  -9.513E-03 -3.400E-03 -4.321E-04
    7  H  3.5535 -1.6803 -0.2274   4.775E-04  3.662E-03 -2.019E-03
    8  H -1.2917 -2.2373 -2.9713   5.511E-03  9.404E-03  9.339E-03
    9  H -0.3216 -3.9176 -0.2274  -3.404E-03  1.435E-03 -2.015E-03
   10  H -3.2319 -2.2373 -0.2274   7.679E-03  6.532E-03 -4.113E-04
  FILE GEO_OPT.xyz EXISTS, NEW DATA WILL BE APPENDED
  ****************************************************************
  *** TOTAL STEP NR.    35           GEOMETRY STEP NR.      1  ***
  *** GNMAX=  7.606342E-02                ETOT=    -36.430486  ***
  *** GNORM=  3.243756E-02               DETOT=     0.000E+00  ***
  *** CNSTR=  0.000000E+00                TCPU=         26.21  ***
  ****************************************************************
    1  2.155E-02   5.129E-03     -35.592831    8.377E-01      0.76
    2  1.535E-02   1.659E-03     -36.334534   -7.417E-01      0.74
    3  4.564E-03   9.334E-04     -36.413279   -7.875E-02      0.76
 ...
 ```
 the calculation stops after the convergence criterion is reached for the `GEMAX` value, and then the positions and forces of the atoms are reported. After that another wavefunction optimization starts. 
* Finally, at the end of the geometry optimization, the final geometry is reported, together with other information, with the breakdown of the total energy among the different components
 ```
 ================================================================
 =              END OF GEOMETRY OPTIMIZATION                    =
 ================================================================



 RESTART INFORMATION WRITTEN ON FILE                  ./RESTART.1

 ****************************************************************
 *                                                              *
 *                        FINAL RESULTS                         *
 *                                                              *
 ****************************************************************

   ATOM          COORDINATES            GRADIENTS (-FORCES)
   1  C  0.2543 -0.4421  0.3391   3.038E-04 -2.422E-04 -3.244E-04
   2  C  2.8160  0.0387 -0.9001  -2.316E-04  9.596E-05 -4.271E-05
   3  C -1.4398 -2.4199 -0.9001   5.144E-05  4.660E-04  1.020E-04
   4  O -0.3892  0.6589  2.2787   3.073E-04  7.982E-05  4.438E-04
   5  H  2.6598  0.1443 -2.9697   9.867E-05 -2.949E-04 -1.140E-04
   6  H  3.6615  1.7764 -0.1552   3.777E-05  1.586E-04 -4.962E-04
   7  H  4.0861 -1.5573 -0.4716  -3.101E-05  1.134E-04  1.259E-04
   8  H -1.4480 -2.2383 -2.9699   2.901E-04 -1.218E-04 -1.119E-05
   9  H -0.6961 -4.3167 -0.4609   3.010E-05  2.301E-04  2.037E-04
  10  H -3.3668 -2.2739 -0.1557  -2.320E-05  2.441E-04 -3.027E-04
 
 ****************************************************************

 
 ELECTRONIC GRADIENT:
    MAX. COMPONENT =    7.84233E-08         NORM =    8.03717E-09
 NUCLEAR GRADIENT:
    MAX. COMPONENT =    4.96207E-04         NORM =    2.31465E-04
 

 TOTAL INTEGRATED ELECTRONIC DENSITY
    IN G-SPACE =                                    24.0000000000
    IN R-SPACE =                                    24.0000000000

 (K+E1+L+N+X)           TOTAL ENERGY =          -36.50473110 A.U.
 (K)                  KINETIC ENERGY =           27.68120068 A.U.
 (E1=A-S+R)     ELECTROSTATIC ENERGY =          -27.60993889 A.U.
 (S)                           ESELF =           29.92067103 A.U.
 (R)                             ESR =            1.70588209 A.U.
 (L)    LOCAL PSEUDOPOTENTIAL ENERGY =          -29.50106253 A.U.
 (N)      N-L PSEUDOPOTENTIAL ENERGY =            3.66785169 A.U.
 (X)     EXCHANGE-CORRELATION ENERGY =          -10.74278205 A.U.
          GRADIENT CORRECTION ENERGY =           -0.58322719 A.U.
 
 ****************************************************************
 ```
* The last section of the output reports the memory allocation and the timing of the run.
 
For more details see [CPMD manual](https://www.cpmd.org/wordpress/CPMD/getFile.php?file=manual.pdf) and [CPMD documentation](https://www.cpmd.org/wordpress/index.php/documentation/#).
<hr>  
</details>


### Dipole moment calculation
Now we want to calculate the dipole of the acetone for the optimized structure at 'zero temperature' just found by using the `RESTART.1` file to retrieve the relevant information from the previous calculation. 
We need a new input file, which is similar to the one used for the optimization, with few important differences:
1. The `&CPMD` section now asks CPMD to compute properties, using the `WAVEFUNCTION` and `COORDINATES` from the `RESTART` file written in the `LATEST` file (those files were generated in the previous calculation)
   ```
   &CPMD
    PROPERTIES
    RESTART WAVEFUNCTION COORDINATES LATEST
   &END
   ```
2. The properties to calculated are specified in the `&PROP` section
   ```
   &PROP
   DIPOLE MOMENT
   &END
   ```

Copy the input in the current folder and run the dipole moment calculation (again, removing `&` if you're using a run script):

```
cp ../../data/dipole.inp .

mpirun -np 2 cpmd.x dipole.inp path-to-mimic-folder/mimic-school/Day1_Acetone_Ex/data/pseudopotentials > dipole.out &
```
This calculation will be much faster than the previous one, and will produce different output files. In particular, the `dipole.out` file contain a `CALCULATE DIPOLE MOMENT` section, reporting the dipole moment along x,y,z axes and the total, both in atomic units and Debye.
This is what you should see:



<p align = "center">
<img src="./data/fig/dipole_output.png" alt="dipole_out" width="700"/>
</p>

## System Preparation for Classical MD with GROMACS
Now we will move to the system preparation using classical MD. Ensure that you either have GROMACS loaded into your coding environment or called upon in your run script. For example:

```
module load GROMACS
```

Create a folder where we are going to prepare the structure inputs and the topology for the acetone tutorial:

```
mkdir  ../2_system_prep
cd ../2_system_prep
```

### Structure File
The structure file for acetone is provided in the `data` folder for this tutorial. Copy this file in the current folder: 

```
cp ../../data/acetone.gro .
```
<details>
<summary>:information_source: Structure files</summary>
<hr> 
 
 `.gro` files contain molecular structure in Gromos87 format, in this case for the acetone molecule:

```
 Acetone molecule gas phase
 10
 1ACT     C1    1   5.041   5.034   5.035
 ...
 10.00000  10.00000  10.00000
```
The first line is the title (free string), the second line is the number of atoms (integer), and each of the following lines refers to one atom, reporting its residue number, residue name, atom name, atom number, position (x,y,z, in nm), and velocity (optional). The very last line contains the box vectors in x,y,z dimensions.

For more details see [GROMACS documentation](https://manual.gromacs.org/documentation/current/reference-manual/file-formats.html#gro).
<hr> 
</details>

### Topology
Also the topology files are provided in the `data` folder for this tutorial. Copy these files in the current folder:  

```
cp ../../data/acetone.top .
cp ../../data/acetone.itp .
```
<details>
<summary>:information_source: Topology files</summary>
<hr> 
 
 `.top` files contain the topology for the system, i.e. the parameters used for the force field, in this case for the acetone molecule (that we will solvate). Topology files contain different sections:

```
; Include forcefield parameters
#include "amber03.ff/forcefield.itp"
#include "acetone.itp"

; Include SPC water topology
#include "amber03.ff/spc.itp"
```
Here different `.itp` files are included. Those files are where the parameters actually are stored. In this case we include parameters from the AMBER force field (`#include "amber03.ff/forcefield.itp"`), containing standard parameters for proteins and  nucleic acids, prameters for acetone (`#include "acetone.itp"`), from the `.itp` file provided that we have just copied in the current folder, and parameters for water. Different water models exist (for more details on this topic see for [this resource](https://water.lsbu.ac.uk/water/water_models.html "water structure and science - Martin Chaplin")), and we choose to use the SPC rigid model, which is a three-site water model, i.e. contains three interaction points corresponding to the three atoms of the water molecule.

```
[ system ]
; Name
Acetone molecule
```
The `[ system ]` section, which needs to be placed after any other level, except the `[ molecules ]` one (here the previous sections are not shown, but are contained in the different `.itp` files). This section contains the name of the system (a string).

```
[ molecules ]
; Compound     #mols
ACT            1
```
The `[ molecules ]` section defines the total number of molecules i the system. Here, for the moment, we have only one acetone molecule. Each name present in this section must correspond to a name given with `[ moleculetype ]` previously in the topology (in our case, in the `.itp` files).  The order of the blocks of molecule types and the numbers of molecules must match the coordinate file (`acetone.gro` in this case).

For more details see [GROMACS documentation](https://manual.gromacs.org/documentation/current/reference-manual/topologies/topology-file-formats.html).
<hr> 
</details>

### Solvation
After this preparatory step, we will now start to actually use GROMACS.
We will solvate the acetone molecule, i.e. add water molecules to our structure. To do this, we can use the `solvate` command of GROMACS. Note if you're writing the execution cmmands with MPI scripts, you should use `gmx_mpi_d` to call on GROMACS. Here we use `gmx` for a single command line request.

```
gmx solvate -cp acetone.gro -cs spc216.gro -p acetone.top  -o solvated.gro -box 2.5 2.5 2.5
```
Where we have specified the water model (redundant here, since the `spc216.gro` model is the default one for the `solvate` command) and the size of the box, namely a cubic box of size 2.5nm.
The solvated structure is `solvated.gro`, and the topology file has been updated backup file with the previous topology is saved as well, named `#acetone.top.1#`). 

:warning: Doublecheck the new topology: the last lines should appear as 
```
ACT      1
SOL        506
```
with a single line per molecule type. If this is not the case and a single line is present
```
ACT      1SOL        506
```
edit the topology as explained aboove with one line per molecule type!

<details>
<summary>:information_source: gmx solvate</summary>
<hr> 
 
The `solvate` command solvates a solute configuration in a box filled with solvent molecules. Different options can be used:

*  `-cp` structure file for the solute, such as a protein
*  `-cs` structure file for the solvent (where `spc216.gro` is the default one, i.e. SPC water model)
* `-p` topology file for the system
* `-box` box size (x,y,z vectors) in nm

This command will produce a new structure file containing the solvated system, and will also update the topology file (keeping a backup version of the input topology). In particular, the `[ molecules ]` section of the topology will now contain
```
[ molecules ]
; Compound        #mols
ACT            1
SOL            506
```
Specifying that 506 solvent molecules, with molecule type `SOL`, which is defined in the water topology file that we already included in the main topology file.

For more details see [GROMACS documentation](https://manual.gromacs.org/documentation/current/onlinehelp/gmx-solvate.html?highlight=solvate
).
<hr> 
</details>
