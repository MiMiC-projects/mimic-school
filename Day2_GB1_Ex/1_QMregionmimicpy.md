# GB1 protein
The system under study for this tutorial is a biologically relevant system: GB1 which is the B1 domain of streptococcal protein G.
It is composed of 56 amino acids arranged in a single α helix and a four-stranded β sheet. GB1 is a promising scaffold for metallo-functionalization and numerous attempts of designing functional metal binding sites have been performed, reporting successful design of zinc, iron, and copper. In particular, the structure used in this exercise is a zinc-binding GB1 mutant, featuring a tetrahedral solvent accessible zinc binding site in one of the loop regions. (For more details on the system, see [J. Am. Chem. Soc. 2018, 140, 13, 4517–4521](https://doi.org/10.1021/jacs.7b10660 "JACS-2018")).

In particular, it is important to note that QM/MM treatment is essential for this system because of the presence of the zinc ion: most classical force fields do not provide an accurate enough description of metal environments since they are not able to take into account explicitly the electronic degrees of freedom.


The QM subsystem of the GB1 system consists of the zinc ion, together with three coordinating histidine side chains and one coordinated water molecule, while the MM subsystem contains the remainder of the protein and more than 8000 water molecules.
 
<p align = "center">
<img src="./data/fig/GB1.jpeg" alt="GB1" width="500"/>
</p>
<p align = "center">
<figcaption align = "center"><a>Zinc-binding site of the GB1 mutant. A possible choice for the QM subsystem is shown here, representing the atoms as ball-and-stick, with yellow balls for boundary atoms. Bulk water is not shown for clarity. <a href="https://pubs.acs.org/doi/full/10.1021/acs.jctc.9b00093" title="mimic_paper">[Image source: J. Chem. Theory Comput. 2019, 15, 6, 3810-3823.]</a></figcaption>
</p>


---


For this tutorial we will assume that you cloned the github repository, hence you will have the provided folders and files in the same locations as the repository.

```
git clone https://gitlab.com/MiMiC-projects/mimic-school.git
```
This command will clone the `mimic-school` repository in the current folder. Navigate the repository and create your own directory to run this exercise 

```
cd mimic-school/Day2_GB1_Ex/
mkdir  solution
cd  solution
```

## QM Region Selection
In this exercise, we will start from a pre-equilibrated structure (provided in the `data` folder). The preliminary steps, starting from a X-ray structure, are analogous to the ones explained in detail in the tutorial for Acetone, and need to be performed for any system. In particular, before running a QM/MM simulation, it is necessary to equilibrate the system at force-field level, with classical MD.

### Basic QM region

To prepare the CPMD and GROMACS inputs, we will use the MiMiCPy package as was discussed in Exercise 1. Please refer to the previous exercise for a detailed explanation on the usage of MiMiCPy. Copy the GROMACS topology and the classically equilibrated structure file to the current directory.

```
mkdir 1_mimicpy
cd 1_mimicpy
cp ../../data/gromacs.gro .
cp ../../data/topol.top .
```

To be able to use MiMiCPy we set up a python virtual environment which needs to be activated and load the modules needed on G100

:warning: we also have an additional command with respect to yesterday, needed to run MiMiCPy on G100 cluster, related to `.vmdrc` file 
```
module purge
module load intel-oneapi-compilers/2021.4.0
module load intel-oneapi-mkl/2021.4.0
module load intel-oneapi-mpi/2021.4.0
module load python/3.8.12--intel--2021.4.0
source /g100_work/tra22_qmmm/Programs/gromacs-2021.5/bin/GMXRC.bash
export GMX_PATH=/g100_work/tra22_qmmm/Programs/gromacs-2021.5/bin/
source $WORK/Programs/gromacs-2021.5/bin/GMXRC.bash

. /g100_work/tra22_qmmm/Programs/mimicpy_venv/bin/activate
export GMXLIB='/g100_work/tra22_qmmm/Programs/GROMACS/share/gromacs' 
cp ../../data/.vmdrc $HOME 
```
Now you can run the MiMiCPy `prepqm` script 
```
mimicpy prepqm -top topol.top -coords gromacs.gro 
```
The command will open an interactive session with the following message printed out:
```
 	                ***** MiMiCPy *****                  

 	 For more information type mimicpy [subcommand] --help 

=====> Running prepqm <=====


**Reading topology**


**Reading coordinates**  /Done

Please enter selection below. For more information type 'help'
> 
```
We can add and delete atoms using the selection language provided by MiMiCPy. Let us add the `ZN` atom and the coordinating water (residue `58`) to create a very basic QM region with the following command:
```
> add name is ZN
> add resid is 58
> q
```
This will then generate a CPMD input script `cpmd.inp` and a GROMACS index file `index.ndx`.

### Optimal QM region
The selection of the QM region is a crucial point in each QM/MM study. The computational cost of a QM/MM calculation is usually dominated by the size of the QM region, hence this should be as small as possible. At the same time, the QM region is the only one where an accurate description of bond breaking (and all the events where the electronic degrees of freedom play a role) is possible, hence it should be large enough to include all the regions that are chemically relevant (i.e. the ones involved in the chemical reaction), or potentially relevant (i.e. the ones whose polarization has an effect on the phenomenon under investigation).

We will try to make different selections for the quantum region, and comment the effects of each of them. Moreover, this will help you getting familiar with MiMiCPy.
1. The selection we made in the previous section by only including the zinc and the coordinating water, is clearly not large enough--any protein residue is excluded. This is not a smart choice to sufficiently describe the interaction of the zinc ion with the protein itself. Let's remove the output generated and make new attempts.
	```
	rm index.ndx cpmd.inp
	mkdir QMregion_tests
	cd QMregion_tests
	```
2. Since we want to describe the interaction between the zinc ion and the protein, another possible choice could be to include in the QM region residues closer to it, within a certain radius. For example, we could use all residues within ~6Å from the zinc ion. Before making the selection in MiMiCPy,  it is a good practice to take a look at the structure. This can be done with different tools, for example VMD.

	To make this easier, especially if you are not familiar with VMD, a VMD visualization state is provided. This allows to save residue selection and visualization commands and helps you srarting from an already good-looking view.
	```
	vmd -e ../../../data/GB1.vmd
	```
	:warning: in case the main  menu is not shown in VMD, you can type  `menu main on` directly into the VMD command line which opens after running the previous command.


	<p align = "center">
	<img src="./data/fig/GB1_visstate.png" alt="GB1" width="500"/>
	</p>
	<p align = "center">
	<figcaption align = "center"><a>Visualization state of GB1 protein. The zinc ion is represented as a sphere.</a></figcaption>
	</p>

	You can change visualization selecting `Graphics > Representations...` in the VMD menu, and this will open the `Graphical Representations` window. What you will see here that a selection in CPK (ball-and-stick) representation is already present. You can make it visible by double clicking on the listed represnetation (its text should turn red when hidden), or you can create a new one by selecting the button `Create Rep`. The suggested selection includes all residues within 6Å of the zinc ions, using the following VMD syntax and hitting your keyboard's `enter` key after typing the text:
	```
	same residue as within 6 of name ZN
	```

	<details>
	<summary>:information_source: VMD selection</summary>
	<hr> 
 	
	VMD provides a rather powerful selection language. Some example of selection commands include `index` (atom number), `name` (atom name), `resid` (residue number), and `resname` (residue name), where names and numbers are the ones present in the coordinate file. Selections can be combined with boolean operators `and` and `or`, collected inside of parenthesis, and modified by `not`. 

	For example, 
	```
	(name CA or name CB) and resid 2
	```
	selects `CA` and `CB` atoms in residue number `2`.

	Helpful selection tools include `within`, which is used as `within <number> of <selection>`, selecting every atom within `<number>`Å (here 6) of the `<selection>` (in our case `name ZN`, i.e. the zinc ion). Moreover, `same residue as` before allows to include the entire residues, even if only some of their atoms are within the required distance. 
	<hr>   
	</details>
	
	
	<p align = "center">
	<img src="./data/fig/GB1_within6A.png" alt="GB1_within6A" width="500"/>
	</p>
	<p align = "center">
	<figcaption align = "center"><a>Residues within 6Å of zinc ion in ball-and-stick representation.</a></figcaption>
	</p>

	By visual inspection the number of selected atoms seems quite large, also including residues less likely interacting with the zinc ion. Indeed, it is possible to easily count the selected atoms using the Tk console of VMD: open the console by selecting `Extensions > TkConsole` in the VMD main menu and count the number of atoms selected with
	```
	set sel [atomselect top "same residue as within 6 of name ZN"]
	$sel num
	```
	resulting in `183` atoms. This selection would indeed result in a too large quantum region: typical QM regions contain less than 200 atoms, but also in this case projects involving QM/MM simulations of that size take months to be finalized. Moreover, as noted before, the current selection involves atoms quite far from the zinc ion, likely not involved in any chemical reaction with it.
	
3. A more reasonable QM region selection consists in choosing only residues in close proximity to zinc, i.e. the coordinating ones. You can try to perform different selections and visualize the corresponding atoms by modifying the graphical representation provided, and counting the number of atoms with the TkConsole.

	Our suggestion is to select residues within 3.5Å of the zinc ion (corresponding to a common cutoff usually chosen for hydrogen bonds). Analogously as before, this can be done in the `Graphical Representations` window with
	```
	same residue as within 3.5 of name ZN
	```
	resulting in the selection of 55 atoms: three histidines and one water molecule. This selection would correspond to quantum box of size much more reasonable than the previous one, hence lowering the computational cost. Moreover, now only residues more likely interacting with the zinc are selected.

	<p align = "center">
	<img src="./data/fig/GB1_within3_5A.png" alt="GB1_within3_5A" width="500"/>
	</p>
	<p align = "center">
	<figcaption align = "center"><a>Residues within 3.5Å of zinc ion in ball-and-stick representation.</a></figcaption>
	</p>

4. Selection 3 corresponds for sure a more treatable QM region, but the amount of resources needed would still be quite large. It is possible to further reduce the computational cost by selecting only the atoms of the residues involved in the reaction with the zinc, i.e. excluding the backbone atoms.

	The choice of the bonds to "cut" when defining a QM region can be, in principle, an arbitrary choice. As a rule of thumb though, any bond to cut should ideally be:
	1. A single bond (σ bond)
	2. Between two atoms with equal or at least very similar electronegativity
	3. Not belonging to conjugated or aromatic moieties (a.k.a. corresponding to π molecular orbitals)
	
	The idea behind these rules of thumb is that the electronic description of the boundary atom will be inevitably poor, hence it shall be avoided to choose an atom involved in bonds where the electrons play a substantial role. When possible, C-C single bonds shall be selected, due to their symmetry and the absence of electronegativity difference. 

	Based on above considerations, think about what atoms you would exclude from selection 3. One way to do that via the `Graphical Representations` window is using boolean operators `and not`
	```
	<selection1> and not (<selection2>)
	```
	where `<selection1>` is the selection of the residues within  3.5Å of zinc ion, and `<selection2>` includes the atoms to exclude from the selection. Atoms can be selected using their index, or the name of their atom type. This can be found in the coordinate file, or more interactively from VMD by pressing `p` on your keyboard(shortcut for 'pick atom') and selecting one atom. This will show in the terminal the information about the `picked atom`, including the `name`, i.e. the atom type we can use for exclusion.

	Our suggestion is to select
	```
	(same residue as within 3.5 of name ZN) and not (protein and (name O N C H HA))
	```
	As you may have noticed, both the backbone oxygen of histidines and the oxygen of the water molecule have name `O`, hence we specified to exclude atom `O` (together with `N`, `C`, `H`, and `HA`) only from the `protein`, i.e. from the histidines.

	This selection reduces the number of QM atoms to 46, corresponding to a smaller QM box needed, and in particular is the same used in the study mentioned at the beginning, from which the first figure is taken (<a href="https://pubs.acs.org/doi/full/10.1021/acs.jctc.9b00093" title="mimic_paper">J. Chem. Theory Comput. 2019, 15, 6, 3810-3823</a>). The cut will be done between Cα and Cβ carbon atoms (with names `CA` and `CB` respectively). Here the selection is shown, coloring in yellow the boundary atoms (Cα atoms of the three histidines) and the bond to cut (Cα-Cβ):

	<p align = "center">
	<img src="./data/fig/GB1_QMregion.png" alt="GB1_QMregion" width="500"/>
	</p>
	<p align = "center">
	<figcaption align = "center"><a>Final selection for the QM region (atoms in ball-and-stick representation, with boundary atoms and Cα-Cβ bond to cut in yellow).</a></figcaption>
	</p>

#### From VMD to MiMiCPy

Having chosen the desired QM region, we will now prepare the GROMACS and CPMD inputs. We will again use MiMiCPy for this. We can note down the residues and atoms that are involved in the QM region we selected in VMD, and write them to a file `sele.dat` in the selection format that MiMiCPy will understand. You can find this file already written in the `/Day2_GB1_Ex/data` directory:

```
add (resid is 5) or (resid is 16) or (resid is 18)
delete (name is CA) or (name is HA) or (name is N) or (name is H) or (name is C) or (name is O)
add-link (resid is 5) and (name is CA)
add-link (resid is 16) and (name is CA)
add-link (resid is 18) and (name is CA)
add resid is 58
add name is ZN
```

<details>
<summary>:information_source: Handling link atoms in MiMiCPy</summary> 
<hr>

Link atoms are those in the QM region, that have one or more bonds with MM atoms. These atoms need to be grouped separately in the CPMD input file, and assigned a special pseuopotential. To correctly write the CPMD input file, MiMiCPy requires information on what atoms the user wishes to mark as link atoms. This can be communicated by adding these atoms using `add-link` instead of the usual `add` in either the interactive session or the `sele.dat` file.

```
add-link (resid is 5) and (name is CA)
```

This will add the atom `CA` of residue 5 as a link atom, and will be placed into a separate pseuopotential block. We can specify the exact pseuopotential file to be used by adding this line to the `pp_info.dat` file:

```
C C_MT_BLYP.psp C_GIA_DUM_AN_BLYP.oecp KLEINMAN-BYLANDER P -
```

This will force MiMiCPy to use the `C_MT_BLYP.psp` pseudopotential file for all "normal" carbon atoms, and the `C_GIA_DUM_AN_BLYP.oecp` pseudopotential file for all link carbon atoms.

<hr>                                                                                                                                                 </details>

We will now present you with a few options for converting the selected QM region atoms into an input readable by the MiMiCPy script which prepares our CPMD input. Please skim through these options and try 1 or 2 which you like best!

You will need to copy some required files to simplify the MiMiCPy selection (you should still be in solution/1_mimicpy/QMregion_tests)

	```
	cp ../../../data/mimic.mdp .
	cp ../../../data/sele.dat .
	cp ../../../data/pp_info.dat .
	cp ../../../data/template.inp .
	cp ../../../data/topol.top .
	cp ../../../data/gromacs.gro .
	```

1. Option 1: Manual conversion <br/>
	Run `mimicpy prepqm` as was described in exercise 1

	```
	mimicpy prepqm -top topol.top -coords gromacs.gro -sele sele.dat -inp template.inp -pp pp_info.dat -mdp mimic.mdp -gmx gmx_mpi_d -tpr mimic.tpr -pad 0.35 -out production.inp
	```
	where we have few new keywords for MiMiCPY to facilitate the process: `-inp` is a template for a input CPMD script, which will changed accordingly to run it with MiMiC, `-pp` contains information about the pseudopotentials used for the atoms, and we are generating the `mimic.tpr` directly  (by adding  `-gmx` and `-tpr`).

2. Option 2: VMD prepqm plugin <br/>
	Although the above procedure is doable, it can be tedious to manually convert the atoms selected in VMD into the selectionlanguage of MiMiCPy especially for larger QM regions. For this, MiMiCPy provides a `prepqm` plugin for VMD and PyMOL. Going back to the VMD environment scroll up to instruction point two to re-launch vmd), if our desired QM region from point 4 is labelled as `sel` in the TK console of VMD, we can pass this on to `prepqm`. Open the TK console in VMD and run:

	```
	set sel [atomselect top "(same residue as within 3.5 of name ZN) and not (protein and (name O N C H HA CA))"]
	```

	This includes all the residues with 3.5Å within the zinc ion, excluding the backbone atoms `O`, `N`, `C`, `H`, and `HA`. This is the QM region we decided in the previous section. We also exclude atom `CA`, which we will pass separately as link atoms. Set a new selection for the link atoms, consisting of only the atoms `CA` of the residues:

	```
	set sel_link [atomselect top "(same residue as within 3.5 of name ZN) and name CA"]
	```
	We now call the prepqm plugin in VMD and pass the selections:

	:warning: Be sure to have followed the right procedure while installing MiMiCPy to make the `prepqm` command available on your local copy of VMD and/or PyMOL. The procedure is available in the `mimicpy_setup.md` file in the main exercise directory. **If you have loaded the provided python virtual environment, you should not worry about this**
	
	```
	prepqm -sele $sel -molid 0 -sele_link $sel_link -q 2 -inp template.inp -out production.inp -ndx index.ndx -top topol.top -pad 0.35 -pp pp_info.dat
	```
	where `-sele` is the selection of the QM atoms, `-molid` is the molecule ID (you can see that from VMD menu), `-sele_link` is the selection for the link atoms, and `-molid` is the charge ofthe QM region (in the "command line version" you could also have added this keyword, otherwise it is estimated from the charges of the atoms, but it should always be double checked!). The remaining commands are analogous the the usual `mimicpy prepqm` command we have used.

	You may see some warining, but if everything worked affectively, after them it should be printed out: 
	```
	Wrote Gromacs index file to index.ndx
	Wrote new CPMD input script to production.inp
	```

3. Option 3: PyMOL prepqm plugin (not for G100 tutorial, but good to know!)<br/>
	An analogous `prepqm` plugin exists also for PyMOL, which may simplofy the process in case you have a preference for that over VMD. Refer to the MiMiCPy manual for more details.

4. Option 4: Rely on MiMiCPy with VMD help <br/>
	MiMiCPy can also automatically assign link atoms for you. It does this by reading the bond information, and marks the atoms with bonds straddling the QM-MM boundary as link atoms. We set our selection as:

	```
	set sel [atomselect top "(same residue as within 3.5 of name ZN) and not (protein and (name O N CH HA))"]
	```

	In this case, the selection includes the `CA` atoms. MiMiCPy will detect that these `CA` atoms are the ones with bonds across the QM-MM interface, and will automatically mark these as QM atoms. We can inform the `prepqm` plugin that we wish to do this by running the following command:

	```
	prepqm -sele $sel -molid 0 -inp template.inp -out production.inp -ndx index.ndx -top topol.top -q 2  -pad 0.35 -pp pp_info.dat -link=True
	```

	In this case we do not need to pass a separate selection for the link atoms. A message, with the list of atoms assigned as link are printed:

	```
	Link atoms were automatically set. The below atoms were marked as link:

	+------------------+
	|  Atom  | Residue |
	+------------------+
	|  82 CA |  5 HE2  |
	+------------------+
	| 286 CA |  18 HD5 |
	+------------------+
	| 255 CA |  16 HD4 |
	+------------------+
	```

	We can cross-check that MiMiCPy guessed the correct atoms as link.

<details>
<summary>:information_source: Automatic detection of link atoms in MiMiCPy</summary> 
<hr>

As mentioned above, MiMiCPy can automatically detect and assign link atoms from the selection that is passed. This is useful for a developing a streamlined procedure for MiMiC input generation. The feature was presented for the VMD plugin, but is also available in PyMOl with a very similar syntax. The feature is also exposed through the command line program, using the `-link` option:

```
mimicpy prepqm -top acetone.top -coords heat.gro -sele sele.dat -mdp mimic.mdp -gmx gmx -tpr mimic.tpr -link True
```

<hr>                                                                                                                                                 </details>


### Gromacs tpr file

Finally, after executing the `prepqm` command, the `production.inp` and `index.ndx` files will be generated. However, the `prepqm` plugin for VMD, unlike the command line version, does not provide an option to generate the `.tpr` file from `index.ndx` automatically. So if you've chosen an option which does not generate the `.tpr` file automatically, you will need to run GROMACS preprocessor either via script or on command line using the output `index.ndx` to generate the `.tpr` file (where we also included in the next command the modules needed to run GROMACS on G100):

```
cp ../../../data/mimic.mdp .
source $WORK/Programs/gromacs-2021.5/bin/GMXRC.bash
module purge
module load intel-oneapi-compilers/2021.4.0
module load intel-oneapi-mkl/2021.4.0
module load intel-oneapi-mpi/2021.4.0
module load python/3.8.12--intel--2021.4.0

gmx_mpi_d grompp -f mimic.mdp  -c gromacs.gro  -p topol.top -o mimic.tpr -n index.ndx 
```
--- 

Now you have seen how the preparation phase can look like  in practice for a "real" system, much more complex than the simple acetone from Day1 ! 
We will use the file generated with MiMiCPy in the next steps. 
