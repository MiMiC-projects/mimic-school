## QM/MM simulations
Now that we have carefully considered different aspects fundamental for any QM/MM simulation, namely the choice of the quantum box and the benchmarking of optimal parameters for running, we are ready to perform an actual QM/MM MD for GB1 mutant using MiMiC.

To save time during this tutorial, we will directly run a production run, using the same starting point of the benchmarks, corresponding to a well-equilibrated configuration for the system. Of course, in the real case, the starting point will be the MM-equilibrated structure. The main steps are analogous to the ones performed on the first tutorial on acetone. We will highlight some important points for QM/MM equilibration in the next section, and then we will perfrom the actual production run. Additionally, we include relevant input and run files for the different phases of QM/MM if you'd like to do each step more explicitly following the directions of the Acetone tutorial adapted for GB1 inputs. 

### QM/MM equilibration
The starting point for a QM/MM simulation will be the equilibrated structure at MM level. As done for acetone, the usual procedure is to perform an annealing simulation, followed by a heating from 0K to the desired temperature. These phases are usually performed using Born-Oppenheimer (BO) MD, since thanks to its stability over Car-Parrinello (CP) MD. Once the system is well equilibrated, it is possible to choose if moving to CP MD or keep using BO MD.

A goal of the equilibration phase is to relax the structure and finally assign velocities to the atoms compatible with the desired temperature. This is expecially important for the quantum atoms, since their equilibrium distances can differ from the onces of the MM equilibrated structure, resulting in a large energy, and hence temperature, overheating the system.

#### Temperature of the QM region (used later!)
The temperature can be extracted from the velocities of each atom (knowing the corresponding mass). However, the CPMD output only reports the temperature of the overall system. In some cases, expecially when equilibrating complex systems, it may be useful to monitor the temperature of the quantum region: a overheating of the quantum region may be not visible from the total system temperature because of the large amount of MM atoms, but it can cause instability in the simulation. To facilitate checking the temperature, you can use the `temp_check.py` script 
```
wget https://gitlab.com/MiMiC-projects/mimic_helper/-/raw/main/scripts/temp_check.py
```
This python script will compute the temperature of the QM and MM atoms for the frames saved in a `TRAJECTORY`. The input needed are also a `GEOMETRY.xyz` file, from which the atom type will be extracted, and the `cpmd.out` file, used to extract the degrees of freedom. The usage of this simple script is
```
python3 temp_check.py TRAJECTORY GEOMETRY.xyz cpmd.out
```
and it will produce a `temp_check.dat` file, reporting the temperature for the QM and MM systems for each step. 

#### QM/MM annealing
The aim of the annealing phase is to obtain a minimal energy structure, gradually removing kinetic energy from the nuclei. In practice, this can be done using the `ANNEALING IONS` in the `&CPMD` section: the ionic velocities will be scaled every time step by a factor specified in the next line. In case of particularly unstable starting structures, i.e. structures minimized at MM level, but resulting in large energy and hence temperatures for the QM subsystem, it may be a good idea to use a strong annealing factor (for example `0.8`). The effect of a strong annealing factor would be to quickly minimize the temperature, without the possibility for the system to moove much. A slow heating will allow the system to relax up to the desired temperature.

As example, here you can see the evolution of the temperature of the GB1 system during the annealing phase. In particular, both the QM and MM temperature are reported here, obtained using the `temp_check.py` script. It is possible to see that with the annealing factor used, both QM and MM temperature drop to ~0K quite quickly. From the inserts in the figure, it is also evident how the total temperature is dominated by MM atoms due to their large number compared to the QM ones. 

<img src="./data/fig/GB1_annealingT.png" alt="annealing" width="600"/>
<figcaption>Evolution of temperature (QM and MM subsystems and total) along the annealing phase. </figcaption>

:warning: Always have a visual look at the system to check everything is behaving correclty: expecially during the first QM/MM steps in the annealing phase, the temperature of the QM atoms may be quite high, with the possibility to break bonds and ending up in abnormal chemical structures that will inevitably be propagated in the subsequent phases. 

#### QM/MM heating 
The aim of the heating phase is to equilibrate the system from a low temperature, ~0K, to the desired one. This can be done by coupling the system to a thermostat, and increasing its temperature linearly at each step. A simple Berendsen-type thermostat can be used in the heating phase: it does not fully preserve the correct canonical ensemble but we are not interested to this feature at this stage, while it is numerically fast and more stable than alternative algorithms. In practice, this can be done using in the `&CPMD` section both the `BERENDSEN IONS` and `TEMPERATURE RAMP` keywords. In particular, `BERENDSEN IONS` couples a Berendsen thermostat to the ions in the system, and two numbers have to be specified on the line below the keyword: the target temperature and the time constant τ of the thermostat (in a.u.). For `TEMPERATURE RAMP` 3 numbers have to be specified on the line below: initial temperature (in K), target temperature (in K) and the ramping speed (in K per atomic time unit). It is a common practice to set a target temperature slightly higher than the real target one, since the thermostat may require a long time before actually reaching the target temperature specified.

As example, here you can see the evolution of the temperature of the GB1 system during the first steps of the heating phase. In particular, both the QM and MM temperature are reported, obtained using the `temp_check.py` script. It is possible to see that also in this case, the total temperature is dominated by MM atoms due to their large number compared to the QM ones. If this was not a big issue in the annealing pahse since both MM and QM temperatures were close to 0K, in this case it would be necessary to equilibrate for longer to stabilize also the temperature of the QM atoms. In particular, just checking the output in the `ENERGIES` file may be misleading: only the temperature for the whole system is reported, but as it is evident from the plot below, the QM atoms may be far from being equilibrated! A good practice is to always check the temperatures of both regions, together with other meaningful quantities. Moreover, having a visual look at the system in a platform like VMD even during the equilibration phases is always a good idea.

<img src="./data/fig/GB1_heatingT.png" alt="heating" width="600"/>
<figcaption>Evolution of temperature (QM and MM subsystems and total) along the first steps of the heating phase. </figcaption>

:warning: Remember that the number of QM atoms is usualy small. For this reason the instantaneous temperature may fluctuate a lot (as you can see for the QM temperature in contrast to the MM one). This is normal and simply due to statistics. However, what should be observed is that the average temperature for the QM atoms stabilizes around a reasonable value, without fluctuating too wildly.

In practice, the example of heating phase reported here would for sure need to be extended in order to equilibrate also the temperature of the QM atoms. Moreover, the total temperature of the MM atoms is quite lower than the target one (here 300K). What can be done, is restarting another heating of the system from the temperature reached, up to the target one (or to a slightly higher one to be on the safe side). Subsequently heating ramps may be necessary to obtain a well equilibrated system, for both the QM and MM atoms, to the desired temperature.

<details>
<summary>:information_source: Tips for Heating Large Systems</summary> 
<hr>

Large biological systems can sometimes stuggle to heat to the same temperature in both subsystems (QM & MM) simulantously. In practice there are ways to support a system like this. One is to alternatively "fix" atoms in the QM region and in the MM region and allow the thermostat to work solely on a the non-fixed region. Another approach is the use of multiple thermostats whic each work separately on different portions of your system. Though we will not use thes methods in this tutorial, the CPMD manual includes instructions for setting up multiple thermostats in the `&CPMD` section and for fixing atoms in the `&ATOMS` section of the CPMD input files. See [CPMD Manual 4.3](https://www.cpmd.org/wordpress/CPMD/getFile.php?file=manual.pdf)).

<hr>                                                                                                                                                 </details>


### QM/MM MD
Now we will launch an NVT production run for our mutant GB1 protein using the parameters obtained from our benchmarking assessments. We provide for you in the `data` folder a `RESTART` file for launching a production run with a pre-heated GB1 system. 

Let's move to another folder to run our production phase run:
```
mkdir ../4_production
cd ../4_production
```
We need to copy in the necessary restart and input files from the `data` folder (or equivalently their path can be provided as input to the script):
```
wget https://gitlab.com/MiMiC-projects/mimic_helper/-/raw/main/scripts/temp_check.py
cp /g100_work/tra22_qmmm/Resources/example_sbatch.sh ./prod_run.sh
cp ../../data/RESTART_forProd ./RESTART
cp ../../data/cpmdinput_forProd.inp ./cpmd.inp
cp ../1_mimicpy/QMregion_tests/production.inp ./cpmd.inp
cp ../1_mimicpy/QMregion_tests/mimic.tpr  ./ 
```
In case you skipped the first section, the last two files are provided and can be copyied in the current folder
```
cp ../../data/cpmdinput_forProd.inp ./cpmd.inp
cp ../../data/mimictpr_forProd.tpr  ./mimic.tpr
```
Before running, verify that your input and run file include the optimal parameters we've identified through benchmarking, that your file path is updated in the `&MIMIC` section of the input file  and `MAXSTEP 100` is set (we will use a very low number of steps to save resources, in practice of course you would like to run longer for production).   Spend some time skimming the input file and note the other keywords you might not be familiar with to review later (more details in the drop down menu below and also at the end of last section!). 

<details>
<summary>:information_source: Additional CPMD Keywords</summary> 
<hr>
You may see additional keyowrds in your CPMD input file than in previous parts of this tutorial. These keywords are certianly optional but we include them here to consider how to make your QM/MM calculations as efficient as possible for larger systems like solvated biomolecules. For more information, please refer to the [CPMD manual online](https://www.cpmd.org/wordpress/CPMD/getFile.php?file=manual.pdf). 

* EXTRAP - Include this in the RESTART line to restart from a previously saved wavefunction history.
* MEMORY BIG - Using BIG, the structure factors for the density cutoff are only calculated once and stored for reuse. This option allows for considerable time savings in connection with Vanderbilt pseudopotentials.
* EXTRAPOLATE WFN STORE - Wavefunctions are used to extrapolate the initial guess wavefunction in Born-Oppenheimer MD. This can help to speed up BO-MD runs significantly. With the additional keyword STORE the wavefunction history is also written to restart files.
* REAL SPACE WFN KEEP - he real space wavefunctions are kept in memory for later reuse. This minimizes the number of Fourier transforms and can result in a significant speedup at the expense of a larger memory use.
* USE_MPI_IO - Specify that MPI shall be used for parallel reading/writing of the RESTART file.
* USE_IN_STREAM - Specify that the RESTART file shall be read in the stream mode. This allows for parallel restarting. Needs the USE_MPI_IO keyword to be written as well.
* USE_OUT_STREAM - Specify that the RESTART file shall be written in the stream mode. This allows for parallel restarting. Needs the USE_MPI_IO keyword to be written as well.
* PARA_USE_MPI_IN_PLACE - Use MPI_IN_PLACE for parallel operation.  Default is FALSE.
* CP_GROUPS - Set the number of groups to be used in the calculation. Default is 1 group. The number of groups is read from the next line and needs to be a divisor of the number of nodes in a parallel run.
<hr>                                                                                                                                                 </details>
Once you're satisfied with your files (remember to adapt the resources and the command line in the sbatch input!) launch your production run:
```
sbatch prod_run.sh
```

You can monitor the CPMD output file:
```
tail -f prod_run.out
```

When the short simulation is completed, we can for example plot the output system temperature and classical energy. Using  gnuplot:
```
module load gnuplot
gnuplot
p 'ENERGIES' u 1:3 w l
```
And system ECLASSIC (total classical MD energy) energy too:
```
p 'ENERGIES' u 1:5 w l
```
You can also use `temp_check.py` script introduced before to monitor separately the temperature of the QM and MM regions. 

In an NVT production run of a well-equilibrated and non-reacting system we would assume both system temperature and total classical MD energy to more or less stay stable. Is this true for our short run of mutant GB1? Do you notice a difference in average time per MD step for acetone and GB1?

If you'd like to see you GB1 system move through the recorded trajectory you can launch VMD and load up your trajectory into the system geometry:
```
module load vmd
vmd GEOMETRY.xyz
```
Highlight your molecule's input on the VMD main menu by clicking on it once (should look slightly highlighted now). Right-click on the molecule name which should be `GEOMETRY.xyz` unless you changed it! Select `Load data into molecule...`. In the Molecule File Browser you can select the `Browse...` button which should take you to your current directory. Double-click on `TRAJECTORY`. Under the dropdown menu for `Determine file type` manually select `CPMD`. Click the `Load` button. You should start to see the frames number increase in your VMD Main menu and your system start to move. You can replay the frames with the arrow controls at the bottom of the VMD Main menu.Feel free to create graphical representations to focus just on the protein or the QM regions. 
