#!/bin/bash
#SBATCH --job-name=analysis
#SBATCH --time=00:40:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=8
#SBATCH --cpus-per-task=1
#SBATCH --partition=g100_usr_prod
#SBATCH --account=tra22_qmmm

module purge  
module load intel-oneapi-compilers/2021.4.0
module load intel-oneapi-mkl/2021.4.0
module load intel-oneapi-mpi/2021.4.0
module load python/3.8.12--intel--2021.4.0

export PATH="/g100_work/tra22_qmmm/Programs/plumed-2.6.5/lib/:$PATH"
export LIBRARY_PATH="/g100_work/tra22_qmmm/Programs/plumed-2.6.5/lib/:$LIBRARY_PATH"
export LD_LIBRARY_PATH="/g100_work/tra22_qmmm/Programs/plumed-2.6.5/lib/:$LD_LIBRARY_PATH"
export PLUMED_KERNEL="/g100_work/tra22_qmmm/Programs/plumed-2.6.5/lib/libplumedKernel.so"

source $WORK/Programs/gromacs-2021.5/bin/GMXRC.bash  
export CPMD_PATH=$WORK/Programs/CPMD
export GMX_PATH=$WORK/Programs/gromacs-2021.5/bin/
export PATH=$CPMD_PATH:$GMX_PATH:$PATH

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export KMP_AFFINITY=compact
export I_MPI_PIN=on

cpmd_input=cpmd
mpirun -n 7 cpmd.x $cpmd_input'.inp' $WORK/Resources/PP > $cpmd_input'.out' : -n 1 gmx_mpi_d mdrun -deffnm mimic -ntomp $OMP_NUM_THREADS
