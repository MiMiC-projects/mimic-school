#!/bin/bash

##########################################################################
# MODIFY THESE LINES ONLY
##########################################################################

#SBATCH --nodes=10           ### Number of nodes
#SBATCH --ntasks-per-node=4  ### Number of MPI processes per node
#SBATCH --cpus-per-task=3    ### Number of OpenMP threads per process

##########################################################################

#SBATCH --job-name=mimic

#SBATCH --time=00:02:00

#SBATCH --partition=g100_usr_prod
#SBATCH --account=tra22_qmmm

module purge
module load intel-oneapi-compilers/2021.4.0
module load intel-oneapi-mkl/2021.4.0
module load intel-oneapi-mpi/2021.4.0
module load python/3.8.12--intel--2021.4.0

PLUMED=/g100_work/tra22_qmmm/Programs/plumed-2.6.5/lib
export LIBRARY_PATH=$PLUMED:$LIBRARY_PATH
export LD_LIBRARY_PATH=$PLUMED:$LD_LIBRARY_PATH
export PLUMED_KERNEL=$PLUMED/libplumedKernel.so

source $WORK/Programs/gromacs-2021.5/bin/GMXRC.bash
export PATH=$WORK/Programs/CPMD:$WORK/Programs/gromacs-2021.5/bin:$PLUMED:$GMX_PATH:$PATH
export PP_LIBRARY_PATH=$WORK/Resources/PP

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export KMP_AFFINITY=compact
export I_MPI_PIN=ON

nNodes=$SLURM_NNODES
nMPIpn=$(echo $SLURM_TASKS_PER_NODE | cut -d'(' -f1)

# CPMD input file prefix (\wo .inp)
cpmd_inp=prod

# Splits nodes between Gromacs and CPMD
nProcs_CPMD=$(( $nMPIpn * $(($nNodes-1)) ))
nProcs_GMX=$nMPIpn

mpirun -n $nProcs_CPMD cpmd.x     $cpmd_inp.inp > $cpmd_inp.out : \
       -n $nProcs_GMX  gmx_mpi_d  mdrun -deffnm mimic


