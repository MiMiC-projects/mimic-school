# Day 2 - GB1 enzyme 
Introductory tutorial to perform QM/MM simulations using MiMiC on a biologically relevant system (GB1 mutant). Efficient use of resources is also discussed.

Main steps:
1. Set up QM box using MiMiCPy  
2. Benchmarking the best parameters (multipole order and cutoff) for the system
3. Benchmarking the best parameters to efficiently use the available resources
3. Perform QM/MM simulation of the system for a biologically rlevant system 

Theoretical prerequisites for this tutorial: 
* Density functional theory (DFT)
* Basis set (localized and plane-waves) 
* Pseudopotentials
* Classical and quantum molecular dynamics (QM/MM)
* Classical force field based molecular dynamics  

:warning: This tutorial assumes the user to have running versions of MiMiC, CPMD and GROMACS. A a python virtual environment to run the `mimicpy` code is also assumed. Instructions to create a python environment can be found in  `mimic-school/mimicpy_setup.md`.

:warning: To run the tutorial clone the git repository in your local machine. Input files needed for this tutorial are located in the `mimic-school/Day2_GB1_Ex/data/` folder.

:warning: This tutorial assumes some familiarity with MiMiC, hence it is suggested to complete Acetone tutorial first.

:information_source: Tutorial based on:
* CPMD tutorial for the BioExcel Summer School on Biomolecular Simulations 2018 in Pula (Italy) from [Bonvin Lab](https://www.bonvinlab.org/education/biomolecular-simulations-2018/CPMD_tutorial/).   
* [Original MiMiC publication](https://doi.org/10.1021/acs.jctc.9b00093 "MiMiC paper"). 

:information_source: Main contributors to the updated version of this tutorial:  
* Andrea Levy [![Orcid](./data/fig/orcid.png)](https://orcid.org/0000-0003-1255-859X)
* Sophia Johnson [![Orcid](./data/fig/orcid.png)](https://orcid.org/0000-0003-4207-4350)   
* Andrej Antalík [![Orcid](./data/fig/orcid.png)](https://orcid.org/0000-0002-8422-8410)
* Bharath Raghavan [![Orcid](./data/fig/orcid.png)](https://orcid.org/0000-0003-0186-2625)
