## Benchmarks
Now that we selected what atoms will be treated at QM level in our QM/MM simulation, two important points to consider are:
1. Optimal parameters for the system, to ensure high accuracy with lowest possible cost.
2. Optimal use of resources, to speed up the simulation by making efficient uses of available resources.

Both points should be considered in deatail when running any simulation.
To simplify the tutorial, we are going to run benchmarks on an already equilibrated structure at QM/MM level. 

Of course, in the practical case, you will start to run QM/MM simulation from an MM equilibrated one. In your own work, you might like to generate a similar starting point to the one we will use today by performing at least one annealing followed by one heating run before a benchmarking test (as done in the acetone tutorial). However, even if for those initial phases the parameters are not properly optimized, it may be not too critical: in fact, it may even be not easy to asses performances on structure which still needs to be equilibrated because longer time may be required to complete each step. This effect is likey to dominate the overall performances observed, hence it is of utmost importance to run benchmarks once a QM/MM equilibrated stracture has been obtained. This will ensure to run as efficiently as possible the production run.

### Multipole order and long-range cutoff
MiMiC supports a generalized version of the approach by [Laio *et al.*]( https://doi.org/10.1063/1.1462041 "JCP-2002"), where the electrostatic interactions between the QM and MM subsystems are split into a short-range (SR) and a long-range (LR) contribution. MiMiC allows, in principle, to use any order of multipoles, leading to an increase in the precision of the LR interactions and thus allowing to use the LR coupling at shorter distances. This will reduce the number of atoms considered in the SR domain, and hence the overall computational cost.

Similarly to what was done in the [original MiMiC publication](https://doi.org/10.1021/acs.jctc.9b00093 "MiMiC paper"), we will investigate how much the short-range cutoff distances can be decreased while still retaining a high accuracy by performing single-point calculations for varying orders of the multipole expansion and short-range cutoff distances. We will compare key properties to calculations where the short-range coupling is used for the entire system, i.e. no LR coupling used. Specifically, we will examine the total energy and forces. Depending on the system under investigation, other properties may be used to evaluate the effects of these two parameters.


To facilitate this phase, we provide a python script to set up the input files and analyze the results of these benchmarks. Let's move to another folder and run these benchmarks:
```
mkdir ../../2_benchmarks_order-cutoff
cd ../../2_benchmarks_order-cutoff
wget https://gitlab.com/MiMiC-projects/mimic_helper/-/raw/main/scripts/benchmarking_order-cutoff.py
```
At first, this script will set up different folders and input files to run single point calculations, taking care of correctly updating paths and files in folders. Some files need to be provided for inputs and these files will be copied or edited in the different folders.

The files needed as input are provided in the `data` folder and can be copied in the current folder (or equivalently their path can be provided as input to the script). In particular, we will use the tpr file and cpmd input generated using MiMiCPy in the previous section and the 
```
cp ../../data/RESTART_forBenchmarks       ./RESTART
cp ../../data/example_run.sh              ./
cp ../1_mimicpy/QMregion_tests/production.inp ./cpmd.inp
cp ../1_mimicpy/QMregion_tests/mimic.tpr  ./
```
In case you skipped the first section, the last two files are provided and can be copyied in the current folder
```
cp ../../data/cpmdinput_forBenchmarks.inp ./cpmd.inp
cp ../../data/mimictpr_forBenchmarks.tpr  ./mimic.tpr
```

The `benchmarking_order-cutoff.py` script needs some standard python libraries that can be easily loaded by using the MiMiCPy virtual environment that we set. In case you don't have it activated, activate is and run the script
```
. /g100_work/tra22_qmmm/Programs/mimicpy_venv/bin/activate
python3 benchmarking_order-cutoff.py ./ RESTART cpmd.inp mimic.tpr example_run.sh -mo 5 7 8 -cr 18 31 49
```
where the optional argument `-mo` and `-cr` specify respectively the multipole orders and cutoff radii to test. In this tutorial, we provide only few values to test on a single snapshot to reduce the number of calculations needed, but in the real case these benchmarks should be repeated on multiple snapshots and for a wider range of multipole orders and cutoff radii. In particular, if the optonal  arguments `-mo` and `-cr` are not provided, the default values will be used:
* Multipoles order from 0 to 8
* 10 cutoff radii in a variable range, depending on the size of the sizes of the total and quantum boxes. In particular, the smallest radius is set to 70% of the QM box (whether or not it is necessary to include in the SR cutoff all of the MM atoms that are inside the QM cell depends on the system), while the largest radius is set to 70% of the total box, i.e. including almost all the MM atoms in the SR cutoff.
                        
All possible combinations of multipole orders and cutoff radii will be compared with a reference calculation, where short-range coupling is used for the entire system.

You will find in the current folder a new folder for the calculation `benchmarking_order-cutoff/` with different subfolders, each containing a proper input file to run the desired single point calculation. The `benchmarking_order-cutoff.py` script have also produced in the current folder a `run_benchmarks.sh` bash script that can be used to run all the single point calculatons
```
./run_benchmarks.sh
```
:warning: in case you face a permission problem in running the previous command, change the permission of the file you created using `chmod` command and rerun
```
chmod 750 run_benchmarks.sh
./run_benchmarks.sh
```
:warning: This command will subsequently sbatch the jobs corresponding to the different combinations of cutoff radius and multipole order and you will see the progress in the command line (`Submitted batch job`, followed by `Done` after a while). If you want to monitor your jobs, you can open a second terminal and log in again to G100 to check them. This set of 10 calculations may take few minutes (about 20 minutes when tested on G100, but it may also depends on the queue time at the moment you run it), this is a good moment to go through the text while the computer perform the calculations and make sure to have understood what we are doing and why!

--- 

When those calculations are completed, you will see in each folder the output file for the different calculations that we performed. It is possible to analyze them, comparing with the reference calculation (make sure that the total time in the run script was sufficient, ending with the different contributions to the energy), by re-running the `benchmarking_order-cutoff.py` script, adding the `--analysis` optional argument. This time the script will analyze the results of the single point calculations. 
```
python3 benchmarking_order-cutoff.py ./ RESTART cpmd.inp mimic.tpr example_run.sh -mo 5 7 9 -cr 18 31 49 --analysis
```
:warning: you may see some `FutureWarning`, but the analysis should anyway complete fine. 

Two output files will be produced, `benchmarking_order-cutoff_energy.dat` and `benchmarking_order-cutoff_force.dat`, containing the comparison with the reference calculation. Another optional argument could have been added, `--plot`, to directly produce plots of energies and forces with matplotlib (which would need to be loaded). Alternatively, we can have a look at the results using any program to visualize those two files, in particular using gnuplot the commands needed to plot the results are:
```
module load gnuplot
gnuplot
```
```
set multiplot layout 1, 2
set xlabel "Cutoff [a.u]"
set ylabel "|ΔE| [a.u.]"
set format y "%.E"
set logscale y 
plot "benchmarking_order-cutoff_energy.dat" u 1:2 w lp title "M.O. 5", "" u 1:3 w lp title "M.O. 7", "" u 1:4 w lp title "M.O. 8"
set xlabel "Cutoff [a.u]"
set ylabel "|Δg| [a.u.]"
set format y "%.E"
set logscale y 
plot "benchmarking_order-cutoff_force.dat" u 1:2 w lp title "M.O. 5", "" u 1:3 w lp title "M.O. 7", "" u 1:4 w lp title "M.O. 8"
```

<div align="center">
  <img src="./data/fig/benchmarking_example.png" alt="cut-rad" width="500"/>
  <figcaption>
    Multipole order - long range cutoff benchmarking example.
  </figcaption>
</div>
<br />

In the real case, more combinations of multiple order and cutoff radius should be tested, ideally on multiple snapshots. This, for example, can be easily done using the `benchmarking_order-cutoff.py` script we provide. Moreover, based on the system, specific properties can be investigated, together with energy and forces. For this reason, the system after each wavefunction optimization for different multipole orders and cutoff radii is saved in a separate folder, and that can be used as starting point to compute further properties.

As example, we report here also the plot resulting from running the `benchmarking_order-cutoff.py` script with default configuration (resulting in a larger number of multipole order and cutoff radii), where also the `--plot` keyword was used, to automatically plot the results (recall that you will need to load matplotlib for the script to automatically plot results). In particular, the force plot presents also a horizontal line corresponding to 1E-6 a.u. since the numerical precision of this property is roughly of this order.   
<div align="center">
  <img src="./data/fig/benchmarking_example_complete.png" alt="cut-rad_comp" width="800"/>
  <figcaption>
    Example of a more complete benchmarking for multipole order - long range cutoff.
  </figcaption>
</div>
<br />

From these more extensive benchmarks and the previous ones performed, a choice of 7 as multipole order and 31 as cutoff radius seem a good compromise between accuracy and efficiency.

:warning:**Make sure to set thes values in the `cpmd.inp` before proceding**



### Efficient use of computational resources

Finally, before proceeding to the actual production run, we will dedicate this section to the discussion of a very important aspect of HPC benchmarking — the efficiency assessment of our simulation setup. This step should never be avoided, since for each large-scale simulation we allocate a significant amount of resources that can be rapidly exhausted without actually achieving much. We can “easily” prevent such unfortunate situations by careful examination of different settings prior to the actual run.


##### CPMD/GMX split

Because the QM region treatment by CPMD is significantly more demanding than the evaluation of MM forces by Gromacs, most of the times, it is perfectly okay to use only a single MPI process to run Gromacs with the remainder being used by CPMD. We would ideally want to use entire nodes, but unfortunately, our current MiMiC build on Galileo requires us to allocate an entire node to Gromacs. To reduce the impact of this nuisance we will spread our processes across many nodes.


##### SLURM HEADER
Move to a new folder and copy some files needed to run a MiMiC run:
```
mkdir  ../3_benchmarks_efficiency
cd ../3_benchmarks_efficiency 
cp ../2_benchmarks_order-cutoff/cpmd.inp ./prod.inp
cp ../2_benchmarks_order-cutoff/mimic.tpr .
cp ../2_benchmarks_order-cutoff/RESTART .
```
:warning:**Make sure that proper values in the `cpmd.inp` found from the previous benchmarks are set, i.e. `MULTIPOLE ORDER`=`7` and `CUTOFF DISTANCE`=`31.0`**

For this tutorial, we provide you with a new submission script, which you can copy from
```
cp ../../data/D2_benchmarks.sh .
```
It is virtually the same as the ones used in previous exercises, but this time we will focus bit more on its header which reads:
```
#SBATCH --nodes=10           ### Number of nodes
#SBATCH --ntasks-per-node=4  ### Number of MPI processes per node
#SBATCH --cpus-per-task=3    ### Number of OpenMP threads per process
```
The total number of cores allocated for this job is then 10\*4\*3=120, with 108 used by CPMD and 12 by Gromacs according to the previously described split. In a few moments, you will explore the effects of different MPI/OpenMP setups and their scaling with increasing number of nodes (only by modifying these three parameters).

##### CPMD

In order to put together a smoothly running calculation, we will focus on tuning the parameters related to CPMD, which is the main driver of MiMiC and its quantum heart (or KS-DFT solver if you fancy a bit drier description). We will therefore follow the tips from the CPMD manual to achieve the optimal performance.

The single most important thing one should get right is the load-balancing problem in the FFT. The dimension of real space grids are always printed in the output on the line
```
REAL SPACE MESH:    180   150   192
```
and these numbers vary between 20 and 300. In parallel processing these are always distributed across MPI processes over the first dimension alone i.e. 180 in our case. This means that the number of processes should be a divisor of 180 and there is no point using more than 180 processes (not entirely true, we will briefly get to that later). It is worth noting though, that as this is the most efficient way to exploit CPMD parallelism, it is often to your advantage to align the largest dimension of an orthorhombic cell with the x-axis (which we did not do here, shame on us!).

<details>
<summary>:information_source: Possible CPMD issues with mesh size</summary>
<hr> 
While preparing this tutorial with an older version of CPMD we noticed an odd message:

`RV30! X REAL SPACE MESH  HAS CHANGED                    180  168`

This is how CPMD behaves when the default mesh dimensions (or those provided by user) are not compatible with the PW cutoff or are not sufficient for FFT routines. It automatically chooses the next bigger valid numbers, in our case (180 instead of 168), and since this happens after the `PARAPARA` section is printed out, it is not reflected there. In this particular case, the issue has disappeared, but it is good to bear in mind if something like this occurs to you sometimes in the future. 
<hr> 
</details>

Now, let’s start our inquiries to find the optimal split between MPI and OpenMP. Let’s take 10 nodes and use 12 cores on each of them, that is 9 nodes for CPMD (180/9 = 20, nice!), which provides six ways of distributing our load: (12, 1), (6, 2), (4, 3), (3, 4), (2, 6), (1, 12) where **(MPI processes, OpenMP threads)**. We will drop the first and the last and try only the four different splits inbetween.

Since we want to evaluate the overall performance during the production run, we should start our benchmarking simulation with already equilibrated structure. As the equilibration alone might be quite demanding, it is always good to come up with reasonable settings before this step, but it does not require extensive benchmarking. Just remember the rule of the thumb: number of processes should divide the number of points on the x-axis of the real space mesh.

:warning: Once we have set everything up (check the path in the cpmd input file!), make sure the CPMD input file will run only for 20 steps
```
MAXSTEP
  20
```
We will evaluate the average walltime per step from the last 10 steps of such run. This way we avoid counting in the overhead from the first few iterations which are often more expensive due to the lack of previous wave functions for the initial guess and wave function extrapolation. Also, we run for only a few steps to avoid spending too much of our precious computational resources (which applies especially to this workshop, so careful!).

:warning: this run may take a while to enter the queue since we are asking for 10 nodes (so far we always asked for 1). A good idea may be to pair up with others and run separately different combinations of MPI processes and OpenMP threads.

After the run is finished, we can then evaluate the mean time for each step by running the following command 
```
tail -n 10 ENERGIES | awk '{sum += $8}END{printf "%.2f\n", sum/10}'  
```
and repeat this step for three remaining splits.  

In the final stage, we will observe how well does our setup scale with the number of cores. This is important because after a certain number, all codes eventually saturate and are thus unable to meaningfully exploit all the additional resources you pour in. So take the (4, 3) split (which you have hopefully established as the optimal one) and run it on 10, 12, 14 and 16 nodes.

:warning: **Again, please make sure the walltime limit is set to 2 minutes!**

You should see a trend resembling the one shown in the left plot (which is finer than what you get):
<div align="center">
  <img src="./data/fig/bench_1scaling.png" alt="cut-rad_comp" width="450"/>
  <img src="./data/fig/bench_2splits.png" alt="cut-rad_comp" width="450"/>
  <figcaption>
    A series of benchmark runs: Walltime per timestep w.r.t. to the number of nodes for a fixed (left) and varying MPI/OpenMP splits (right).
  </figcaption>
</div>
<br />

You can see here that for the first few values, time per timestep decreases very rapidly, between 4- and 8-node runs almost three-fold. This is partially an artefact of allocating an entire node to Gromacs. The dashed gray lines indicate values with perfect plane distribution and indeed you can notice a sudden drop between 9&rarr;10 and 15&rarr;16. If you open the output for (4,3) split, you should see something like this:
```
PARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARA
  NCPU     NGW     NHG  PLANES  GXRAYS  HXRAYS ORBITALS Z-PLANES
     0    4123   32994       5     150     596       2       1
     1    4125   32994       5     150     596       1       1
     2    4125   32986       5     150     596       2       1
     3    4125   32992       5     150     596       1       1
     4    4125   33000       5     150     596       2       1
     .       .       .       .       .       .       .       .
     .       .       .       .       .       .       .       .
     .       .       .       .       .       .       .       .
    31    4132   32994       5     148     596       2       1
    32    4130   32986       5     148     596       1       1
    33    4130   32994       5     148     596       2       1
    34    4126   32990       5     148     596       1       1
    35    4126   32996       5     148     596       2       1
                G=0 COMPONENT ON PROCESSOR :    20
 PARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARAPARA
```
The fourth column labelled `PLANES` shows the distribution of real space mesh slabs among the processes and it seems to be perfectly uniform.

The right plot then shows effects of different splits for varying numbers of nodes. Let's look just at the (6,2) and (4,3) lines. Moving from 8 to 10 nodes, we can once again notice a drop in (4,3) line, due to the optimal split resulting in 5 planes per process. Yet, it gets more interesting when we move from 10 to 12 nodes, where the time difference decreases by nearly 0.9 s for (6,2), but only by 0.1 s for (4,3). Now why is that?

If we print out the `PARAPARA` section again we get our answer right away. While the distribution for (6,2) shifts from the mix of 3-4 to 2-3 planes per process, in case of (4,3) it is almost exclusively 4, except for a small number of processes that are still assigned 5. And that is enough to hamper any significant speedup. If you will still have some time at the end of this exercise, try running a few of these runs to see for yourself.

One last example to emphasize the importance of load-balancing in CPMD. The plot below illustrates that with clever partitioning of your resources you can achieve significantly better results with just half of them. If we just naively double the number of nodes for the (4, 3) split that seemed to work perfectly, we achieve only a miniscule speedup with respect to the expended resources. One of the reasons is that at 32 we no longer have optimal distribution of real mesh planes (180/(31\*4) = 1.45), while on the other hand, if we switch to (12, 1) split we get a perfect distribution of 1 plane per process that allows us can achieve even better results with just 16 nodes. Moreover, if we want to go bigger, the more appropriate choice seems to be to start from this point and just double the number of OpenMP threads.

In general, if possible, try experimenting with different setups on the target amount of computational resources, but always try to favor MPI parallelism to OpenMP.

<div align="center">
  <img src="./data/fig/bench_3large.png" alt="cut-rad_comp" width="500"/>
  <figcaption>
    An example illustrating the importance of optimal MPI/OpenMP split for a medium-scale resource allocation. The number of OpenMP threads for orange bars is displayed above them.
  </figcaption>
</div>
<br />

When going towards the truly large-scale jobs, it is worth mentioning the keyword CP_GROUPS (which we kind of foreshadowed before). This is a CPMD implementation of additional parallel bands, which allows to break the scaling limit of 1 plane per process. For further details please refer to the respective sections of the [CPMD manual](https://www.cpmd.org/wordpress/CPMD/getFile.php?file=manual.pdf). 



<details>
<summary>:information_source: Further CPMD Keywords worth exploring</summary> 
<hr>
In the input files for this execise, you may have notice that we also silently added few new CPMD keywords. These have be added to improve the performance. You can find a brief description below,  please refer to the [CPMD manual online](https://www.cpmd.org/wordpress/CPMD/getFile.php?file=manual.pdf) for more information. 

* `MEMORY BIG` - Using BIG, the structure factors for the density cutoff are only calculated once and stored for reuse. This option allows for considerable time savings in connection with Vanderbilt pseudopotentials.
* `EXTRAPOLATE WFN STORE` - Wavefunctions are used to extrapolate the initial guess wavefunction in Born-Oppenheimer MD. This can help to speed up BO-MD runs significantly. With the additional keyword `STORE` the wavefunction history is also written to restart files.
* `REAL SPACE WFN KEEP` - he real space wavefunctions are kept in memory for later reuse. This minimizes the number of Fourier transforms and can result in a significant speedup at the expense of a larger memory use.
* `CP_GROUPS` - Set the number of groups to be used in the calculation. Default is 1 group. The number of groups is read from the next line and needs to be a divisor of the number of nodes in a parallel run.
<hr>  

 

