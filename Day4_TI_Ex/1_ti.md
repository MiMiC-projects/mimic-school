# Proton Transfer in β-Endoglucanase
In this exercise, the system under study is the Bacillus 1,3-1,4-β-glucanase enzyme. This enzyme belongs to family 16 glycoside hydrolase and it catalyzes the degradation of 1,3-1,4-β-glucan polysaccharides. The Glu105 residue is important in the catalytses of the polysaccharide degradation by β-endoglucanase (For more details on the system and mechanism of catalysis, see [J. Am. Chem. Soc. 2011, 133, 50, 20301–20309](https://pubs.acs.org/doi/abs/10.1021/ja207113e)). Glu105 acts as a nucleophile, with its negative charge stabilized by hydrogen bonds with nearby residues. One of these residues is Asp107, and the hydrogen bond between Glu105 and Asp107 is particularly interesting as the proton between them can jump from one residue to the other at room tempertaure. The low free energy barrier of this proton transfer is studied using thermodynamic integration (TI) or blue moon ensemble method.

<p align = "center">
<img src="./data/fig/ti_system_figure.png" alt="TI" width="500"/>
</p>
<p align = "center">
<figcaption align = "center"><a>Glu and Asp involved in proton transfer of the β-Endoglucanase enzyme. The QM subsystem of choice is shown here, representing the atoms as ball-and-stick, with yellow balls for boundary atoms. Green balls correspond to the backbone atoms for the amino acids, and are treated at MM level (as the rest of the protein, in green cartoon representation, and water, not shown for clarity). </a></figcaption>
</p>

For this tutorial we will assume that you cloned the github repository, hence you will have the provided folders and files in the same locations.

```
git clone https://gitlab.com/MiMiC-projects/mimic-school.git
```
This command will clone the `mimic-school` repository in the current folder. Navigate the repository and create your own directory to run this exercise 

```
cd mimic-school/Day4_TI_Ex/
mkdir solution
cd solution
```

## Input Preparation
For convenience, the system has been equilibrated at the classical and the QM/MM levels. We can start running thermodynamics integration from the `RESTART` file provided in the `data/` directory. In our new CPMD input script, we will need to inform CPMD that we would like to restart from a previous run and not start a new one. This command needs to be added to the `&CPMD` section:

```
RESTART COORDINATES VELOCITIES WAVEFUNCTION
```

CPMD will then read the initial coordinates, velocities and wavefunctions from the `RESTART` file provided before running the simulation. TI involves running a series of simulations were a chosen collective variable (or CV) is constrainted/restrainted to different values in each simulation. The CV is a descriptor of the system that can drive it along the path we wish to study. We wish to study the proton jumping from the oxygen of Asp107 to the oxygen of Glu105. Often in these cases, we use the difference between distances as the CV. However in this tutorial, the distance between the proton and the accepting oxygen of Glu105 is choosen as a CV. This may not be accurate enough for an actual study of the system, but it is done here to keep things simple. If you would like to repeat this tutorial and obtain realistic values of the free energy, we would suggest to change this CV to the difference of distance. In this case, it would be the difference between the Glu105 oxygen and hydrogen distance and the Asp107 oxygen and hydrogen distance.

In this tutorial we carry out TI by imposing harmonic restraints on the system using CPMD patched with PLUMED. To activate PLUMED, include the `PLUMED` keyword in the CPMD input script.

:warning: remember to use the version of the cpmd executable patched with plumed

This will cause CPMD to look for a `plumed.inp` input script in the current directory. In `PLUMED`, we can use the following code snippet to apply restraints on the distance between two atoms:

```
dist: DISTANCE ATOMS=id1,id2
restraint: RESTRAINT ARG=dist AT=d KAPPA=k
FLUSH STRIDE=5
PRINT ARG=dist,restraint.* STRIDE=5 FILE=COLVAR
```

<details>
<summary>:information_source: Adding constaints directly into cpmd.inp file (without plumed)</summary>
<hr> 

In this tutorial we use the sophisticated plumed setup to set up constrained simulations. However, another way to perform constrained simulations is to add a `CONSTRAINTS` block to the end of the `&ATOMS` section in the CPMD input script. The following code imposes a distance constraint on the system:

```
&ATOMS
.
.
.
CONSTRAINTS
FIX STRUCTURE
1
DIST id1 id2 d
END CONSTRAINTS
END

&END
```

Here, `id1` and `id2` are the CPMD indices of the atoms between which the constraint is imposed and `d` is the distance at which these atoms have to be held. We will discuss how to choose these numbers in the section below. As opposed to the restraints applied in PLUMED these are full constraints, where the CV is forced to be exactly at the specificed value. This is different from adding restraints, where a large force is applied to attempt to fix the CV in a specific range. Effectively, this means that with CPMD constraints we add another constraint that the velocity of evolution of the constraint itself is zero. This can be thought of as adding another constraint, one that is not part of the defintion of the free energy. Furthermore, this is not present when using just restraints. For select CVs (e.g., bond length) this is not a problem, but can be a problem with more complex CVs. For more information, see [J. Phys. Chem. B. 2000, 104, 4, 823-835](https://pubs.acs.org/doi/full/10.1021/jp992590x).

<hr> 
</details>

In the plumed input, first define a variable `dist`, which is the the CV defining the distance between our desired atoms with CPMD indices `id1` and `id2`. We then define a harmonic restraint, which will hold the atoms at distance `d` with force constant `k`. This force constant will have to be chosen wisely, which we will describe in the next section. Lastly, we tell PLUMED to print out the distance and restraint information in a `COLVAR` file with a stride of 5. Often while running MiMiC patched with PLUMED, there is a delay in I/O operations and this data is not written to the COLVAR file immediately. This can cause loss of data if a premature crash occurs, especially while writing restart files are involved (for eg., the `HILLS` file in the case of Metadynamics runs). So we tell PLUMED to flush all print commands with a stride of the same frequency as the printing to the `COLVAR` file.

### Visualize the QM region

To choose the CV, you would need to visualize the system. This can be done quickly by using the full protein gro file. But, often you need to only visualize the QM region and not the whole protein. You can extract this information from the CPMD input script using the `cpmd2coords` subcommand of MiMiCPy:

```
mimicpy cpmd2coords -top data/topol.top -inp data/cpmd.inp -coords qm.gro
```

Here, pass the topology, the cpmd input containing the QM atom coordinates, and the output gro/pdb file with the `-coords` flag. This tool is useful, among other things, for debugging the QM region specificed in the cpmd input.

Visualize `qm.gro` in VMD, and take a note of the residue ID and the atom names of the hydrogen and oxygen involved in your CV. We would need this information in the next section.

### CPMD Atom Indices

In both cases, we are required to input the CPMD indices of the atoms involved in TI. Since the QM atoms are ordered by atomic elements in the CPMD input script, it is not trivial to obtain the atom index of a specific atom. But, we can quickly whip up a python script to do by importing MiMiCPy as a python library. For convenience, a sample script is provided in the `data/` folder as `cpmdidx.py`. But a detailed explanation of the code, demonstrating how MiMiCPy can be accessed through python for more powerful operations, is given in the section below.

<details>
<summary>:information_source: MiMiCPy as a python library</summary> 
<hr>

MiMiCPy can be interfaced through the command line, and through the VMD/PyMOL plugins. But it can be also be directly accesed as a python library. This can be useful to develop python workflows for MiMiC, and to deal with more complex cases of CPMD inputs and MM topologies. We use the case of obtaining the CPMD index of a certain atom as an example to demonstrate how to use the library in python. Load your favorite python interpreter, and import the mimicpy library:

```
import mimicpy
```

The `MiMiCPy Topology` class or the `MPT` class handles all operations related to topologies. We load our GROMACS topology as follows:

```
topol = mimicpy.Mpt.from_file(‘topol.top’)
selection = topol.select(‘resid is 107 and name is HD2’)
```

We select the desired atoms from the topology object `topol` using the `select()` function. This function returns a `pandas` dataframe of the atoms selected, with details like atom name, reside ID, etc. accesible through the columns. The GROMACS indices of the selected atoms are obtained using `selection.index`. To convert this to CPMD indices, load the CPMD input script as a MiMiCPy `CpmdScript` object:

```
cpmd = mimicpy.CpmdScript.from_file(‘cpmd.inp’)
```

The CPMD input file contains the `OVERLAPS` block within the `&MIMIC` section, and this contains the list of MM indices and corresponding CPMD indices for all QM atoms. This information can be accessed using `cpmd.MIMIC.OVERLAPS` parameter of the `CpmdScript` object. The `OVERLAPS` information is then converted to a python dictionary:

```
ids = {int(i.split()[1]):int(i.split()[3]) for i in cpmd.MIMIC.OVERLAPS.splitlines()[1:]}
```

And, finally use this to convert our GROMACS indices to CPMD indices:

```
for i in selection.index:
    print(ids[i])
```

<hr>
</details>

Launch the sample script and pass the cpmd input, topology and the MiMiCPy selection of the atoms we want the CPMD index for (required files can be found in the `data/` folder):

```
python cpmdidx.py -c data/cpmd.inp -t data/topol.top resid is 107 and name is HD2
```

This command should output the CPMD index of the hydrogen atom being donated by the Asp107 residue. Similarly, to find the index of the accepting oxygen of Glu105, run the following:

```
python cpmdidx.py -c data/cpmd.inp -t data/topol.top resid is 105 and name is OE2
```

### PLUMED parameters

When applying a harmonic restraint with PLUMED, we need to input a value of the force constant `KAPPA`. This has to be choosen such that, essentially, the condition that the CV is forced to be very close to the target value should be satisfied. But, it not should be too high to push the system to unphysical configurations. For a simple distance between two atoms as the CV, you can use the harmonic approximation of the bond that would be formed by pushing the two atoms together. If we know the angluar frequency $`\omega`$ associated with this bond, according to the law of harmonic oscillator, this is related to force constant $`K`$ by:

```math
\omega = \sqrt{K/\mu}
```

where $`\mu = m_O m_H/(m_O + m_H)`$ is the reduced mass of the bond. Thus, by this back of the envelope calculation, you can get a reasonable first guess for $`K`$. In this example, the CV chosen is the distance between the hydrogen from Asp107 and the oxygen of Glu105. The wavenumber of the O-H is around $`k = 3000-3500cm^{-1}`$. Using $`k=3500cm^{-1}`$ and $`\omega = 2 \pi c k`$, a force constant $`K = 409212.838 amu/ps^2`$ is got. You can now set the value of `KAPPA` in the PLUMED input file (the value is set slightly higher than the calculated result).  Also set AT=0.19 (this fixes the CV at 1.9Å) by which you constrain the CV with the value in the equilibrium structure (in the RESTART file):

:warning: remember that plumed by default accepts distances in nanometers, so specifiy the distance as 0.19 nm to fix the CV at 1.9Å. The default units of the force constant are in $`amu/ps^2`$. More information [here](https://www.plumed.org/doc-v2.7/user-doc/html/_u_n_i_t_s.html)).

```
restraint: RESTRAINT ARG=dist AT=0.19 KAPPA=409300
```

For TI, you need to calculate the mean force required to restraint the CV at the target value. When applying harmonic restraints, the following formula can be used to calculate the force:

```math
F = \langle k(\theta(r) - q) \rangle
```

Here $`theta(r)`$ is the value of the collective variable as a function of the positions of the total atoms of the system, $`q`$ is the target value of the CV, and $`\langle ... \rangle`$ is a 'constrainted' ensemble average.


## Running TI

As mentioned previously, the MiMiC TI runs are started from the provided RESTART file, containing the equilibriated protein structure. A series of MiMiC simulations, with the selected CV fixed at different values are to be performed. Since the process being studied is the transfer of the HD2 hydrogen of Asp107 to OE2 oxygen of Glu105, the simplest CV that can be chosen is the distance between these two atoms. First, fix the value of the CV to the initial value, in this case 0.19 nm. Use the following `plumed.inp` file:

```
dist: DISTANCE ATOMS=id1,id2
restraint: RESTRAINT ARG=dist AT=0.19 KAPPA=409300

FLUSH STRIDE=5
PRINT ARG=dist,restraint.* STRIDE=5 FILE=COLVAR
```

The value of `KAPPA` is chosen according to the calculation discussed earlier. You should ideally run the simulations for enough time to sample the space sufficiently. However, that might be too expensive for this summer school. Adjust the `MAXSTEP` in `cpmd.inp` depending on the time you have.

Once the simulation is completed, you can calculate the mean force. You need to find this after the system has been sufficiently equilibraited. Discard the first part of the trajectory, i.e, before the value of the CV starts oscillating around the present target value, and then find the mean force. We can quickly do this using the script `forces.py`:

```
python3 ../data/force.py COLVAR 0.19 10
```

Here, you pass the COLVAL filename, the distance we wished to restraint the CV to (in nm), and the time (in ps) upto which to discard. This should print out the mean force. Keep a note of this value.

The simulation would output another restart file `RESTART.1`, using which you can start a new simulation with a different value of the CV. Make a new folder, and move the restart file over:

```
mv ../run1/RESTART.1 RESTART
```

Change the value of the restraining distance to 1.7Å in the plumed file. Now, follow a similar procedure to obtain the average force for this value of CV. Repeat this procedure for values of CV 1.5, 1.3, 1.1 and 1Å. Having obtained the mean force in each case, we can now calculate the free energy profile. Write down the calculated forces in a text file `forces.dat` like so:

```
0.19 <force1>
0.17 <force2>
.
.
.
``` 

The integral of the mean force, or the area under the graph calculated using trapezoidal or Simpsons rule (use the numpy.trapz or scipy.integrate.simps python functions), will yield the free energy profile, with which one can identify the barrier to the proton transfer. We can use the `fes.py` python script to calculate the free energy, passing the forces file we wrote down:
```
python3 data/fes.py forces.dat
```

This should output `fes.dat`, which contains the free energy profile data in kcal/mol. Plot this using gnuplot to observe the behavior.
