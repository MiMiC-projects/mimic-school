import sys
import numpy as np

f = sys.argv[1]
q0 = float(sys.argv[2])
time = float(sys.argv[3])

with open(f, 'r') as f:
    q = np.array([i.split()[:2] for i in f.readlines()[1:]], dtype=float)
q_ = q[q[:,0]>time]
print((np.mean(q_[:,1])-q0)*409300)