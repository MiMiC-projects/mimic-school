import sys
import numpy as np

f = sys.argv[1]

a = np.loadtxt(f, delimiter=' ')
x = a[:,0]
y = a[:,1]

g = [np.trapz(y[:i], x[:i])/4.184 for i in range(1,len(x)+1)]

result = np.column_stack((x,g))

np.savetxt('fes.dat', result, delimiter=' ')
