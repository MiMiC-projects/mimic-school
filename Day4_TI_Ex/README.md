# Day 4 - Thermodynamic Integration with PLUMED

Tutorial to perform thermodynamic intergation using MiMiC and PLUMED, to find the free energy profile of the proton transfer between Asp107 and Glu105 in β-glucanase.

Main steps:
1. Calculate the dipole moment of acetone in vacuum
2. Solvate the system and equilibrate it with classical MD
3. Prepare the input for MiMiC and perform a QM/MM MD simulation at room temperature
4. Calculate the average dipole moment from selected snapshots of the production run
5. Estimate the difference between the dipole moment in vacuum and the one at room temperature to understand the effects of solvent and temperature 

Theoretical prerequisites for this tutorial: 
* Density functional theory (DFT)
* Basis set (localized and plane-waves) 
* Pseudopotentials
* Classical and quantum molecular dynamics (QM/MM)
* Classical force field based molecular dynamics
*  Thermodynamics intergation

:warning: This tutorial assumes the user to have running versions of MiMiC, CPMD (patched with PLUMED) and GROMACS. An environment to run the `mimicpy` code is also assumed. Instructions for compiling the required codes and to run `mimicpy` can be found in the  `mimic/setup` folder.  

:warning: To run the tutorial clone the git repository in your local machine. Input files needed for this tutorial are located in the `mimic/Day4_TI_Ex/data/` folder.

:warning: This tutorial assumes a fair bit of familiraity with MiMiC, hence it is suggested to complete Exercise 1 and 2 first.

:information_source: Tutorial based on:
* Exercise 2 and exercise 4 from the CECAM Summer School ["Hybrid Quantum Mechanics / Molecular Mechanics (QM/MM) Approaches to Biochemistry and Beyond"](https://www.cecam.org/workshop-details/1030) - May 2022, Lausanne (Switzerland) by M. Boero[![Orcid](./data/fig/orcid.png)](https://orcid.org/0000-0002-5052-2849), P. Campomanes[![Orcid](./data/fig/orcid.png)](https://orcid.org/0000-0001-9229-8323),  C. Rovira[![Orcid](./data/fig/orcid.png)](https://orcid.org/0000-0003-1477-5010), and A. P. Seitsonen
* Discussion on thermodynamic integration with Simone Meloni [![Orcid](./data/fig/orcid.png)](https://orcid.org/0000-0002-3925-3799)
* Testing of the tutorial by Sonata Kvedaraviciute [![Orcid](./data/fig/orcid.png)](https://orcid.org/0000-0001-8432-9142)

:information_source: Main contributors to this tutorial:   
* Bharath Raghavan [![Orcid](./data/fig/orcid.png)](https://orcid.org/0000-0003-0186-2625)

